# On Board Software

On Board Software of the **Radar Recognition of Chaff Clouds** (**R2C2**) experiment for the launch campaign of the BEXUS 28/29 edition. The [REXUS/BEXUS](http://rexusbexus.net/) programme is a student programme from **DLR** and **SNSA**, the German and Swedish space agencies, in collaboration with **ESA**, which allows students from universities and higher education colleges across Europe to carry out scientific and technological experiments on research rockets and balloons.  
The experiment successfully flew in **October 2019**.

## Experiment explanation

The R2C2 (Radar Recognition of Chaff Clouds in the stratosphere) experiment concerns the **navigation of high altitude aerostatic balloons**. To navigate such balloons, it is critical to know which wind directions are available in the close vicinity of the balloon and with which intensity the wind is flowing. As of now, the only possible way to determine how the wind layers are moving around the balloon is through trial and error. This means changing the altitude of the balloon in the hopes of finding a wind current that will take it in the desired direction. The primary objective of this experiment is **to determine the speed and direction of the wind layers below the balloon** and **to analyze the gathered data in order to determine the feasibility of this method** to navigate balloons at high altitude. To do this, chaff will be used. Chaff consists of small conductive metal pieces. When released mid-air it forms a “cloud” and slowly drifts away following the air currents before eventually dispersing. This chaff cloud can be tracked with a radar and by studying the dynamics of chaff clouds, the wind direction and speed can be derived, establishing an in-time wind layer model. This experiment would be focused on atmospheric research.

## Architecture

The On Board software of the experiment is divided in three layers: the **mind**, the **r2c2_libraries** and the **arduino libraries**. There is also an third party library, [Time](https://github.com/PaulStoffregen/Time), that is included here for an easier use of git, as some of the members of the team have not had previous experience using it.  


### Mind

It contains the business logic of the software on board the experiment that flew on the BEXUS 29 stratospheric balloon. There are two main modes _passive_ and _active_, being the latter separated in two submodes, _manual_ and _autonomous_. The idea is that the experiment runs in an active/manual mode, and the active/autonomous mode is only used in the unlikely event of losing connection with the gondola during flight.

### R2C2 libraries

Six libraries were implemented in order to encapsulate the different functionalities that the on board software needed to perform:
- r2c2_actuator_lib: contains the physical information to communicate with each of the actuators of the experiment, and allows the control over them.
- r2c2_communication_lib: sets the communication over TCP (commanding) and UDP (telemetry) with the ground station.
- r2c2_data: contains the status of the experiment and the sensor data.
- r2c2_safety_lib: allows to check that all the predefined safety rules are fulfilled and defines the autonomous flow. It is composed of a Safety and a Autonomous classes.
- r2c2_sensor_lib: contains the physical information to communicate with each of the sensors of the experiment, receiving their data and allowing requests to them.
- r2c2_store_lib: stores in a SD card the data from the telemetry and the logs that record the execution flow.

## Coded for

This code has been designed to run in an **Arduino Due** with an **Arduino Ethernet module** to communicate through TCP/IP and store the recorded data and the logs in an SD card. It runs in parallel with a **Raspberry Pi 4**, which controls an on board camera and is commanded from the **Arduino Due** through 2 GPIO pins.

## Authors

This code has been designed by Guillermo Zaragoza Prous.  
This code has been implemented by Guillermo Zaragoza Prous with the help of Gustav Olofsson.  
This code has been tested by the R2C2 team.  

## Contact

R2C2 Web: https://www.r2c2bexus.com/  
Guillermo Zaragoza Prous gitlab account: https://gitlab.com/mrzaragoza