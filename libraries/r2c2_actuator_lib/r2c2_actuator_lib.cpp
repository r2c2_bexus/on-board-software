#include "r2c2_actuator_lib.h"


Actuator::Actuator(Store * store): _store(store)
{

}

ActuatorImpl::ActuatorImpl(Store * store): Actuator(store)
{
    setupMotorDispositive(MotorAddr::MOTOR_1);
    setupMotorDispositive(MotorAddr::MOTOR_2);

    setupServoDispositive(ServoAddr::SERVO_1);
    setupServoDispositive(ServoAddr::SERVO_2);
    setupServoDispositive(ServoAddr::SERVO_3);

    setupCameraManagerDispositive();

    setupHeaterDispositive(HeaterAddr::HEATER_1);
    setupHeaterDispositive(HeaterAddr::HEATER_2);
    setupHeaterDispositive(HeaterAddr::HEATER_3);
    setupHeaterDispositive(HeaterAddr::HEATER_4);
    setupHeaterDispositive(HeaterAddr::HEATER_5);
}

bool ActuatorImpl::isMotorOn(const unsigned int address)
{
    bool isOn = false;
    switch (address)
    {
        case MotorAddr::MOTOR_1:
            isOn = _isMotor1On;
            break;
        case MotorAddr::MOTOR_2:
            isOn = _isMotor2On;
            break;
    }
    return isOn;
}

void ActuatorImpl::motor(const unsigned int address, const bool on)
{
    if (on)
    {
        digitalWrite(address, HIGH);
    }
    else
    {
        digitalWrite(address, LOW);
    }

    switch (address)
    {
        case MotorAddr::MOTOR_1:
            _isMotor1On = on;
            break;
        case MotorAddr::MOTOR_2:
            _isMotor2On = on;
            break;
    }
}

bool ActuatorImpl::isServoOn(const unsigned int address)
{
    bool isOn = false;
    switch (address)
    {
        case ServoAddr::SERVO_1:
            isOn = _isServo1On;
            break;
        case ServoAddr::SERVO_2:
            isOn = _isServo2On;
            break;
        case ServoAddr::SERVO_3:
            isOn = _isServo3On;
            break;
    }
    return isOn;
}

void ActuatorImpl::servo(const unsigned int address, const bool on)
{
    unsigned int angle = on ? _servoOnAngle : _servoOffAngle;

    switch (address)
    {
        case ServoAddr::SERVO_1:
            _servo1.write(angle);
            _isServo1On = (angle == _servoOnAngle);
            break;
        case ServoAddr::SERVO_2:
            _servo2.write(angle);
            _isServo2On = (angle == _servoOnAngle);
            break;
        case ServoAddr::SERVO_3:
            _servo3.write(angle);
            _isServo3On = (angle == _servoOnAngle);
            break;
    }
}

bool ActuatorImpl::isCameraManagerSystemOn()
{
    return _isCameraManagerPowerOn;
}

void ActuatorImpl::cameraManagerPower(const bool on)
{
    if (on)
    {
        digitalWrite(CameraManagerPowerAddr::CAMPOWER_1, HIGH);
    }
    else
    {
        digitalWrite(CameraManagerPowerAddr::CAMPOWER_1, LOW);
    }
    _isCameraManagerPowerOn = on;
}

bool ActuatorImpl::isCameraManagerSystemCommandedToRecord()
{
    return _isCameraManagerRecordingOn;
}

void ActuatorImpl::cameraManagerRecord(const bool on)
{
    if (on)
    {
        digitalWrite(CameraManagerRecordingAddr::CAMREC_1, LOW);
    }
    else
    {
        digitalWrite(CameraManagerRecordingAddr::CAMREC_1, HIGH);
    }
    _isCameraManagerRecordingOn = on;
}

bool ActuatorImpl::isHeaterOn(const unsigned int address)
{
    bool isOn = false;
    switch (address)
    {
        case HeaterAddr::HEATER_1:
            isOn = _isHeaterOn[0];
            break;
        case HeaterAddr::HEATER_2:
            isOn = _isHeaterOn[1];
            break;
        case HeaterAddr::HEATER_3:
            isOn = _isHeaterOn[2];
            break;
        case HeaterAddr::HEATER_4:
            isOn = _isHeaterOn[3];
            break;
        case HeaterAddr::HEATER_5:
            isOn = _isHeaterOn[4];
            break;
    }
    return isOn;
}

void ActuatorImpl::heater(const unsigned int address, const bool on)
{
    if (on)
    {
        digitalWrite(address, HIGH);
    }
    else
    {
        digitalWrite(address, LOW);
    }

    switch (address)
    {
        case HeaterAddr::HEATER_1:
            _isHeaterOn[0] = on;
            break;
        case HeaterAddr::HEATER_2:
            _isHeaterOn[1] = on;
            break;
        case HeaterAddr::HEATER_3:
            _isHeaterOn[2] = on;
            break;
        case HeaterAddr::HEATER_4:
            _isHeaterOn[3] = on;
            break;
        case HeaterAddr::HEATER_5:
            _isHeaterOn[4] = on;
            break;
    }

}

void ActuatorImpl::setupMotorDispositive(int address)
{
    pinMode(address, OUTPUT);
    switch (address)
    {
        case MotorAddr::MOTOR_1:
            _isMotor1On = false;
            break;
        case MotorAddr::MOTOR_2:
            _isMotor2On = false;
            break;
    }

    motor(address, false);
}

void ActuatorImpl::setupServoDispositive(int address)
{
    switch (address)
    {
        case ServoAddr::SERVO_1:
            _servo1.attach(address);
            _isServo1On = false;
            break;
        case ServoAddr::SERVO_2:
            _servo2.attach(address);
            _isServo2On = false;
            break;
        case ServoAddr::SERVO_3:
            _servo3.attach(address);
            _isServo3On = false;
            break;
    }

    servo(address, false);
}

void ActuatorImpl::setupCameraManagerDispositive()
{
    pinMode(CameraManagerPowerAddr::CAMPOWER_1, OUTPUT);
    cameraManagerPower(false);

    pinMode(CameraManagerRecordingAddr::CAMREC_1, OUTPUT);
    cameraManagerRecord(false);
}

void ActuatorImpl::setupHeaterDispositive(int address)
{
    pinMode(address, OUTPUT);
    switch (address)
    {
        case HeaterAddr::HEATER_1:
            _isHeaterOn[0] = false;
            break;
        case HeaterAddr::HEATER_2:
            _isHeaterOn[1] = false;
            break;
        case HeaterAddr::HEATER_3:
            _isHeaterOn[2] = false;
            break;
        case HeaterAddr::HEATER_4:
            _isHeaterOn[3] = false;
            break;
        case HeaterAddr::HEATER_5:
            _isHeaterOn[4] = false;
            break;
    }

    heater(address, false);
}