/*
  r2c2_data.h - Library in charge of communicate with all the actuators on board of R2C2 BEXUS experiment.
  Created by Guillermo Zaragoza Prous, Aug 22, 2019.
  Released into the public domain.
*/

#ifndef Actuator_lib_h
#define Actuator_lib_h

#include <Arduino.h>
#include <Servo.h>
#include "r2c2_store_lib.h"

class MotorAddr
{
public:
    enum
    {
        MOTOR_1 = 31,
        MOTOR_2 = 33
    };
};

class ServoAddr
{
public:
    enum
    {
        SERVO_1 = 8,
        SERVO_2 = 9,
        SERVO_3 = 7
    };
};

class HeaterAddr
{
public:
    enum
    {
        HEATER_1 = 23, // 12 V
        HEATER_2 = 25, // 12 V
        HEATER_3 = 27, // 12 V
        HEATER_4 = 29, // 12 V
        HEATER_5 = 22  // 28 V
    };
};

class CameraManagerPowerAddr
{
public:
    enum
    {
        CAMPOWER_1 = 24
    };
};

class CameraManagerRecordingAddr
{
public:
    enum
    {
        CAMREC_1 = 26
    };
};

class Actuator
{
public:

    Actuator(Store * store);

    virtual bool isMotorOn(const unsigned int address) = 0;
    virtual void motor(const unsigned int address, const bool on) = 0;

    virtual bool isServoOn(const unsigned int address) = 0;
    virtual void servo(const unsigned int address, const bool on) = 0;

    virtual bool isCameraManagerSystemOn() = 0;
    virtual void cameraManagerPower(const bool on) = 0;

    virtual bool isCameraManagerSystemCommandedToRecord() = 0;
    virtual void cameraManagerRecord(const bool on) = 0;

    virtual bool isHeaterOn(const unsigned int address) = 0;
    virtual void heater(const unsigned int address, const bool on) = 0;

protected:

    Store *_store;

};

class ActuatorImpl : public Actuator
{
public:

    ActuatorImpl(Store * store);

    bool isMotorOn(const unsigned int address);
    void motor(const unsigned int address, const bool on);

    bool isServoOn(const unsigned int address);
    void servo(const unsigned int address, const bool on);

    bool isCameraManagerSystemOn();
    void cameraManagerPower(const bool on);

    bool isCameraManagerSystemCommandedToRecord();
    void cameraManagerRecord(const bool on);

    bool isHeaterOn(const unsigned int address);
    void heater(const unsigned int address, const bool on);

private:

    void setupMotorDispositive(int address);
    void setupServoDispositive(int address);
    void setupCameraManagerDispositive();
    void setupHeaterDispositive(int address);

    static const unsigned int _servoOnAngle  = 165;
    static const unsigned int _servoOffAngle = 25;

    Servo _servo1, _servo2, _servo3;

    bool _isServo1On, _isServo2On, _isServo3On;
    bool _isMotor1On, _isMotor2On;
    bool _isCameraManagerPowerOn;
    bool _isCameraManagerRecordingOn;
    bool _isHeaterOn[5];
};

#endif // Actuator_lib_h