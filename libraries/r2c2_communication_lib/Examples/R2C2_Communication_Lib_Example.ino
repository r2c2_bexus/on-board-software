#include <r2c2_communication_lib.h>
#include <r2c2_store_impl_lib.h>
#include <r2c2_data.h>
 
bool connected = false;
  
Store* store = NULL;
Communicate* comm = NULL;

String fileName = "COMM";
const int fileDuration = 25;

StatusData statusData;
SensorData sensorData;
  
void setup() {

  Serial.begin(9600);  
  Serial.println("SETUP");


  store = new StoreImpl(fileName, fileDuration, INFO_LEVEL);
  Serial.println("A");
  comm  = new Communicate(store);
  Serial.println("B");

  byte         mac[6]      = {0xde, 0xaa, 0x3f, 0x8d, 0x22, 0xa3};
  unsigned int ip[4]       = {192, 168, 1, 1};
  unsigned int localPort   = 6666;
  unsigned int remoteIp[4] = {192, 168, 1, 2};
  unsigned int remotePort  = 7777;
  Serial.println("C");
  
  comm->prepareConnection(mac, ip, localPort, remoteIp, remotePort);
  Serial.println("D");
  
  // put your setup code here, to run once:
  connected = (comm->startConnection() == COMM_OK);
  Serial.println("E");
}

void loop() {

  if (!connected)
  {
    //Serial.println("LOOP - Not connected");
    connected = comm->startConnection() == COMM_OK;
    Serial.println("F");
  }
  else
  {
    
    //Serial.println("LOOP - Connected");
    char data[UDP_TX_PACKET_MAX_SIZE];
    
    int sendSensorDataResult = comm->sendSensorData(sensorData);
    
    int sendStatusDataResult = comm->sendStatusData(statusData);
    Serial.println("G");
  }

}
