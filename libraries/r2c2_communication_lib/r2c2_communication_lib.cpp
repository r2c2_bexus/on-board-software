/*
  r2c2_communication_lib.h - Library for communication on board of R2C2 BEXUS experiment.
  Created by Guillermo Zaragoza Prous, April 23, 2019.
  Released into the public domain.
*/

#include <r2c2_communication_lib.h>
#include <SD.h>
#include <cmath>

void printBits(byte myByte){
    for(byte mask = 0x80; mask; mask >>= 1){
        if(mask  & myByte)
            Serial.print('1');
        else
            Serial.print('0');
    }
    Serial.println("");
}

Communicate::Communicate(Store *store): _store(store)
{
}

CommunicateImpl::CommunicateImpl(Store *store): Communicate(store), _n_message(0), _numBytesReceived(0), _isUDPConnected(false)
{
    for (int i = 0; i < (HEADER_SECTION_LENGTH + COMMAND_DATA_SECTION_LENGTH); ++i) {
        _lastReadBytes[i] = 0x00;
    }
    _timeLastMessageReceived = now();
}

int CommunicateImpl::prepareConnection(byte mac[6], unsigned int ip[4], byte dns[4], byte gateway[4], byte subnet[4],
                                   unsigned int localTCPPort, unsigned int localUDPPort, unsigned int remoteIp[4], unsigned int remoteUDPPort)
{
	// TODO: check the memcpy.
	//memcpy(_mac, mac, sizeof(mac));
	
	_ip = IPAddress(ip[0], ip[1], ip[2], ip[3]);
    _localTCPPort = localTCPPort;
	_localUDPPort = localUDPPort;

	_remoteUDPIp = IPAddress(remoteIp[0], remoteIp[1], remoteIp[2], remoteIp[3]);
	_remoteUDPPort = remoteUDPPort;



	// The SD card slot must be initialised to be able to use the Ethernet module.
	if (_store == NULL)
    {
        SD.begin(4);
    }

	//Ethernet.begin(_mac, _ip);
	Ethernet.begin(mac, _ip, dns, gateway, subnet);
    _tcpServer = new EthernetServer(_localTCPPort);

	return 0;
}


int CommunicateImpl::startConnection()
{
    int status = COMM_OK;

  	// Check for Ethernet hardware present
  	if (Ethernet.hardwareStatus() == EthernetNoHardware) 
	{
        _store->log(ERROR_LEVEL, "Ethernet shield was not found.  Sorry, can't run without hardware.");
		_isUDPConnected = false;
        status = COMM_HARDWARE_ERROR;
  	}

  	if (status == COMM_OK && Ethernet.linkStatus() == LinkOFF)
	{
        _store->log(ERROR_LEVEL, "Ethernet cable is not connected.");
        _isUDPConnected = false;
        status = COMM_LINK_OFF_ERROR;
  	}

	if (status == COMM_OK && _isUDPConnected && _client)
	{
        _store->log(INFO_LEVEL, "Ethernet connected.");
        status = COMM_OK;
	}

	if (status == COMM_OK && !_isUDPConnected)
    {
        _udp.begin(_localUDPPort);
        _isUDPConnected = true;
        _store->log(INFO_LEVEL, "UDP running.");
    }

    if (status == COMM_OK && !_client)
    {
        _tcpServer->begin();
        _client = _tcpServer->accept();
        _store->log(INFO_LEVEL, "TCP running.");
    }

    if (status == COMM_OK && _isUDPConnected && _client)
    {
        _store->log(INFO_LEVEL, "Ethernet connected.");
    }
	return status;
}

bool CommunicateImpl::isUplinkConnected()
{
    bool connected = false;

    time_t currentTime = now();
    unsigned int currentTimeSeconds = hour(currentTime) * 3600 + minute(currentTime) * 60 + second(currentTime);
    unsigned int timeLastPintReceivedSeconds = hour(_timeLastMessageReceived) * 3600 + minute(_timeLastMessageReceived) * 60 + second(_timeLastMessageReceived);

    if (currentTimeSeconds < timeLastPintReceivedSeconds)
    {
        currentTimeSeconds += 24 * 3600;
    }

    unsigned int timeDiff = currentTimeSeconds - timeLastPintReceivedSeconds;
    /*Serial.println("Current Time in Seconds: " + String(currentTimeSeconds));
    Serial.println("Last Ping Received in Seconds: " + String(timeLastPintReceivedSeconds));
    Serial.println("Last Ping Difference Time: " + String(timeDiff));*/


    static unsigned int timeoutCounter = 0;
    if (_client && !_client.connected())
    {
        _store->log(ERROR_LEVEL, "Client is not connected. Disconnecting...");
        _client.stop();
    }
    else if (_client && timeDiff > TIMEOUT_DESCONECTION && (++timeoutCounter % TIMEOUT_DESCONECTION) == 0)
    {
        _store->log(ERROR_LEVEL, "Connection timeout reached. Disconnecting...");
        _client.stop();
        timeoutCounter = 0;
    }
    else if (_client)
    {
        connected = true;
        //Serial.println("Client is connected.");
    }
    return connected;
}


int CommunicateImpl::sendData(const byte data[], int length)
{
    int status = COMM_OK;
    if(_isUDPConnected)
    {
        _udp.beginPacket(_remoteUDPIp, _remoteUDPPort);
        _udp.write(data, length);
        _udp.endPacket();
    }
    else
    {
        status = COMM_NOT_CONNECTION_ERROR;
    }
	return status;
}

int CommunicateImpl::sendSensorData(const SensorData* sensorData)
{
    byte message[(HEADER_SECTION_LENGTH + SENSOR_DATA_SECTION_LENGTH)];

    for (int j = 0; j < (HEADER_SECTION_LENGTH + SENSOR_DATA_SECTION_LENGTH); ++j) {
        message[j] = 0x00;
    }

    int header_size = createHeader(message, SENSOR_DATA);
    if (header_size != HEADER_SECTION_LENGTH)
    {
        _store->log(ERROR_LEVEL, "ERROR: Send Sensor Data: The size of the created header is not equal to HEADER_SECTION_LENGTH");
        _store->log(ERROR_LEVEL, "Header size = " + String(header_size));
        return -1;
    }

    int byte_index = HEADER_SECTION_LENGTH;

    unsigned int integersToPositive = 127;
    for (unsigned int i = 1; i <= NUM_TEMPERATURES; i++) {
        unsigned int temp = (unsigned int) ((sensorData->getTemperature(i) + integersToPositive) * 100);
        message[byte_index++] = lowByte(temp);
        message[byte_index++] = highByte(temp);
    }

    unsigned int humidity = (unsigned int) (sensorData->getHumidity() * 100);
    message[byte_index++] = lowByte(humidity);
    message[byte_index++] = highByte(humidity);

    float f_longitude = sensorData->getLongitude();
    uint32_t i_longitude = (uint32_t) ((f_longitude + 180) * 100000);
    byte *b_longitude = (byte*) &i_longitude;
    for (unsigned int i = 0; i < sizeof(i_longitude); i++) {
        message[byte_index++] = b_longitude[i];
    }

    float f_latitude = sensorData->getLatitude();
    uint32_t i_latitude = (uint32_t) ((f_latitude + 180) * 100000);
    byte *b_latitude = (byte*) &i_latitude;
    for (unsigned int i = 0; i < sizeof(i_latitude); i++) {
        message[byte_index++] = b_latitude[i];
    }

    message[byte_index++] = lowByte(sensorData->getAltitude());
    message[byte_index++] = highByte(sensorData->getAltitude());

    message[byte_index++] = lowByte(sensorData->getPressure());
    message[byte_index++] = highByte(sensorData->getPressure());

    uint32_t encoder1Pulses = sensorData->getEncoder1Pulses();
    byte *b_encoder1Pulses = (byte*) &encoder1Pulses;
    for (unsigned int i = 0; i < sizeof(encoder1Pulses); i++) {
        message[byte_index++] = b_encoder1Pulses[i];
    }

    uint32_t encoder2Pulses = sensorData->getEncoder2Pulses();
    byte *b_encoder2Pulses = (byte*) &encoder2Pulses;
    for (unsigned int i = 0; i < sizeof(encoder2Pulses); i++) {
        message[byte_index++] = b_encoder2Pulses[i];
    }

    message[byte_index] = 0x00;
    unsigned int container1Status = (sensorData->getContainer1Detected()) ? CONT_DETECTED : CONT_NOT_DETECTED;
    message[byte_index] = message[byte_index] | ((container1Status & 0b00000001) << 7);
    unsigned int container2Status = (sensorData->getContainer2Detected()) ? CONT_DETECTED : CONT_NOT_DETECTED;
    message[byte_index] = message[byte_index] | ((container2Status & 0b00000001) << 6);
    unsigned int container3Status = (sensorData->getContainer3Detected()) ? CONT_DETECTED : CONT_NOT_DETECTED;
    message[byte_index] = message[byte_index] | ((container3Status & 0b00000001) << 5);
    byte_index++;

    addChecksum(message, byte_index);

    sendData(message,byte_index);

	return 0;
}

int CommunicateImpl::sendStatusData(const StatusData* statusData)
{
    byte message[(HEADER_SECTION_LENGTH + STATUS_DATA_SECTION_LENGTH)];

    for (int j = 0; j < (HEADER_SECTION_LENGTH + STATUS_DATA_SECTION_LENGTH); ++j) {
        message[j] = 0x00;
    }

    int header_size = createHeader(message, STATUS_DATA);
    if (header_size != HEADER_SECTION_LENGTH)
    {
        _store->log(ERROR_LEVEL, "ERROR: Send Sensor Data: The size of the created header is not equal to HEADER_SECTION_LENGTH");
        _store->log(ERROR_LEVEL, "Header size = " + String(header_size));
        return -1;
    }

    int byte_index = HEADER_SECTION_LENGTH;

    // First Byte of data
    Experiment_Status experimentStatus = statusData->getExperimentStatus();
    message[byte_index] = message[byte_index] | ((experimentStatus & 0b00000011) << 6);
    Container_Status container1Status =	statusData->getContainerStatus(1);
    message[byte_index] = message[byte_index] | ((container1Status & 0b00000011) << 4);
    Container_Status container2Status =	statusData->getContainerStatus(2);
    message[byte_index] = message[byte_index] | ((container2Status & 0b00000011) << 2);
    Container_Status container3Status =	statusData->getContainerStatus(3);
    message[byte_index] = message[byte_index] | ((container3Status & 0b00000011));
    byte_index++;

    // Second Byte of data
    for (int i = 1; i <= NUM_TEMPERATURES; i++) {
        Sensor_Status tempStatus = statusData->getTemperatureSensorStatus(i);
        message[byte_index] = message[byte_index] | ((tempStatus & 0b00000001) << (8-i));
    }
    Sensor_Status humStatus = statusData->getHumiditySensorStatus();
    message[byte_index] = message[byte_index] | ((humStatus & 0b00000001) << 1);
    Sensor_Status gpsStatus = statusData->getGPSStatus();
    message[byte_index] = message[byte_index] | ((gpsStatus & 0b00000001));
    byte_index++;

    // Third Byte of data
    Sensor_Status pressureStatus = statusData->getPressureStatus();
    message[byte_index] = message[byte_index] | ((pressureStatus & 0b00000001) << 7);
    for (int i = 1; i <= NUM_MOTORS; i++) {
        Actuator_Status motorStatus = statusData->getMotorStatus(i);
        message[byte_index] = message[byte_index] | ((motorStatus & 0b00000001) << (7-i));
    }
    for (int i = 1; i <= NUM_SERVOS; i++) {
        Actuator_Status servoStatus = statusData->getServoStatus(i);
        message[byte_index] = message[byte_index] | ((servoStatus & 0b00000001) << (5-i));
    }
    Camera_Status cameraStatus = statusData->getCameraStatus();
    message[byte_index] = message[byte_index] | ((cameraStatus & 0b00000011));
    byte_index++;

    // Fourth Byte of data
    State_Machine_Status stateMachineStatus = statusData->getStateMachineStatus();
    message[byte_index] = message[byte_index] | ((stateMachineStatus & 0b00000011) << 6);
    unsigned int lastContainerReleased = statusData->getLastContainerReleased();
    message[byte_index] = message[byte_index] | ((lastContainerReleased & 0b00000011) << 4);

    if (NUM_HEATERS > 4)
    {
        for (int i = 1; i <= 4; i++) {
            Actuator_Status heaterStatus = statusData->getHeaterStatus(i);
            message[byte_index] = message[byte_index] | ((heaterStatus & 0b00000001) << (4-i));
        }
    }
    else
    {
        for (int i = 1; i <= NUM_HEATERS; i++) {
            Actuator_Status heaterStatus = statusData->getHeaterStatus(i);
            message[byte_index] = message[byte_index] | ((heaterStatus & 0b00000001) << (4-i));
        }
    }
    byte_index++;

    // Fifth Byte of data
    Actuator_Status heater5Status = statusData->getHeaterStatus(5);
    message[byte_index] = message[byte_index] | ((heater5Status & 0b00000001) << 7);
    Sensor_Status imuStatus = statusData->getIMUStatus();
    message[byte_index] = message[byte_index] | ((imuStatus     & 0b00000001) << 6);
    Communication_Status commStatus = statusData->getCommunicationStatus();
    message[byte_index] = message[byte_index] | ((commStatus    & 0b00000001) << 5);
    byte_index++;


    // Sixth and Seventh Bytes of data
    time_t t = statusData->getLastContainerReleasedTime();
    int16_t hour_min = hour(t) * 100 + minute(t);
    message[byte_index++] = lowByte(hour_min);
    message[byte_index++] = highByte(hour_min);



    // Eighth and Ninth Bytes of data
    int16_t sec_mil  = second(t) * 100;
    message[byte_index++] = lowByte(sec_mil);
    message[byte_index++] = highByte(sec_mil);



    // Tenth and Eleventh Bytes of data
    unsigned int release_gap = statusData->getReleaseGapTime();
    message[byte_index++] = lowByte(release_gap);
    message[byte_index++] = highByte(release_gap);

    addChecksum(message, byte_index);

    sendData(message,byte_index);

	return 0;
}

bool CommunicateImpl::commandAvailable()
{
	bool newCommand = false;

    if(!_client)
    {
        _store->log(INFO_LEVEL, "No client connected to TCP port. Trying to connect...");
        _client = _tcpServer->accept();
    }

	if(_client)
    {
	    if(_client.available() >= HEADER_SECTION_LENGTH)
        {
            for (int i = 0; i < (HEADER_SECTION_LENGTH + COMMAND_DATA_SECTION_LENGTH); ++i) {
                _lastReadBytes[i] = 0x00;
            }
            _numBytesReceived = 0;
            for (_numBytesReceived; _numBytesReceived < (HEADER_SECTION_LENGTH + COMMAND_DATA_SECTION_LENGTH); _numBytesReceived++) {
                _lastReadBytes[_numBytesReceived] = _client.read();
            }

            if((_lastReadBytes[6] & 0x60) >> 5 == 0x02) // It is a command
            {
                newCommand = true;
            }
            else if((_lastReadBytes[6] & 0x60) >> 5 == 0x03) // It is a ping
            {
                newCommand = true;
            }
            else // It is neither a command nor a ping
            {
                for (int i = 0; i < (HEADER_SECTION_LENGTH + COMMAND_DATA_SECTION_LENGTH); ++i) {
                    _lastReadBytes[i] = 0x00;
                }
                _numBytesReceived = 0;
            }
        }
    }

	return newCommand;
}


int CommunicateImpl::processMessage(byte& commandCode, uint16_t& releaseGap, StatusData * statusData)
{
    int status = COMM_OK;
    if(_numBytesReceived > 0) {
        _store->log(TRACE_LEVEL,"There is a message!");

        if((_lastReadBytes[6] & 0x60) >> 5 == 0x02) // It is a command
        {
            commandCode = _lastReadBytes[7];
            _store->log(INFO_LEVEL, "Command received: " + String(commandCode));

            releaseGap = (_lastReadBytes[8] << 8) + _lastReadBytes[9];

            if(commandCode  == CMD_SET_LAUNCH_TIME)
            {
                _store->log(INFO_LEVEL, "Set Launch Time.");

                unsigned hr_min = (_lastReadBytes[0] << 8) + _lastReadBytes[1];
                unsigned int hr = hr_min / 100;
                unsigned int min = hr_min % 100;
                unsigned sec_mil = (_lastReadBytes[2] << 8) + _lastReadBytes[3];
                unsigned int sec = sec_mil / 100;

                statusData->resetExperimentTime(hr, min, sec, true);
                _store->log(INFO_LEVEL, "Time set to: " + String(hr) + ":" + String(min) + ":" + String(sec));
            }

            _timeLastMessageReceived = now();
        }
        else if((_lastReadBytes[6] & 0x60) >> 5 == 0x03) // It is a ping
        {
            _store->log(INFO_LEVEL, "Ping received");
            if(year(_timeLastMessageReceived) < YEAR && !statusData->isReleasing())
            {
                unsigned hr_min = (_lastReadBytes[0] << 8) + _lastReadBytes[1];
                unsigned int hr = hr_min / 100;
                unsigned int min = hr_min % 100;
                unsigned sec_mil = (_lastReadBytes[2] << 8) + _lastReadBytes[3];
                unsigned int sec = sec_mil / 100;

                statusData->resetExperimentTime(hr, min, sec);
                _store->log(INFO_LEVEL, "Time set to: " + String(hr) + ":" + String(min) + ":" + String(sec));
            }
            _timeLastMessageReceived = now();
            commandCode = PING;
        }
        else // It is neither a command nor a ping
        {
            status = COMM_WRONG_MESSAGE_ERROR;
        }

    }
    else
    {
        _store->log(TRACE_LEVEL,"No message!");
        status = COMM_NO_MESSAGE_ERROR;
    }

	return status;
}

int CommunicateImpl::createHeader(byte *header, TypeMessage type)
{
    int header_index = 0;

    time_t t = now();
    int16_t hour_min = hour(t) * 100 + minute(t);
    header[header_index++] = lowByte(hour_min);
    header[header_index++] = highByte(hour_min);

    int16_t sec_mil  = second(t) * 100;
    header[header_index++] = lowByte(sec_mil);
    header[header_index++] = highByte(sec_mil);

    header[header_index++] = lowByte(_n_message);
    header[header_index++] = highByte(_n_message++);

    byte last_header_byte = 0b0000000;
    bitWrite(last_header_byte, SENDING_ADDRESS_BIT, MIND);
    bitWrite(last_header_byte, TYPE_MESSAGE_FIRST_BIT, type);
    header[header_index++] = last_header_byte;

    return header_index;
}


int CommunicateImpl::addChecksum(byte *message, unsigned int length)
{
	int sum = 0;

    for (int i = 0; i < length; ++i) {
        sum += (int) message[i];
    }
	sum = sum % ((int) pow(2,CHECKSUM_BITS));

    message[(HEADER_SECTION_LENGTH -1)] = message[(HEADER_SECTION_LENGTH -1)] + lowByte(sum);
	return sum;
}


