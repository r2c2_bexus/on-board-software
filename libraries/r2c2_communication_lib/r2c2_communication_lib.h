/*
  r2c2_communication_lib.h - Library for communication on board of R2C2 BEXUS experiment.
  Created by Guillermo Zaragoza Prous, April 23, 2019.
  Released into the public domain.
*/
#ifndef Communication_lib_h
#define Communication_lib_h

#include "Arduino.h"
#include <EthernetUdp.h>
#include <Ethernet.h>
#include <TimeLib.h>

#include <r2c2_communication_protocol.h>
#include "r2c2_store_lib.h"
#include "r2c2_data.h"

#define UDP_TX_PACKET_MAX_SIZE 34 // Default size is 24 bytes in the EthernetUdp library.

#define COMM_OK			    0
#define COMM_HARDWARE_ERROR	1
#define COMM_LINK_OFF_ERROR	2
#define COMM_NOT_CONNECTION_ERROR	3
#define COMM_NO_MESSAGE_ERROR	4
#define COMM_WRONG_MESSAGE_ERROR	5

class Communicate
{
	public:

		Communicate(Store * store);

		// Sets all the necessary variables for the communication between the arduino and other system.
        virtual int prepareConnection(byte mac[6], unsigned int ip[4], byte dns[4], byte gateway[4], byte subnet[4],
                unsigned int localTCPPort, unsigned int localUDPPort, unsigned int remoteIp[4], unsigned int remoteUDPPort) = 0;
		
		// Function to control and start a UDP connection. It must be used after prepareConnection, and must be called at least once before sending or receiving data.
        virtual int startConnection() = 0;

        virtual int sendSensorData(const SensorData* sensorData) = 0;
        virtual int sendStatusData(const StatusData* statusData) = 0;

		//bool isThereNewMessage();
        virtual bool commandAvailable() = 0;

        virtual int processMessage(byte& commandCode, uint16_t& releaseGap, StatusData * statusData) = 0;

        virtual bool isUplinkConnected() = 0;

	protected:
		Store *_store;

};

class CommunicateImpl : public Communicate
{
public:

    CommunicateImpl(Store * store);

    int prepareConnection(byte mac[6], unsigned int ip[4], byte dns[4], byte gateway[4], byte subnet[4],
                          unsigned int localTCPPort, unsigned int localUDPPort, unsigned int remoteIp[4], unsigned int remoteUDPPort);

    int startConnection();

    int sendSensorData(const SensorData* sensorData);
    int sendStatusData(const StatusData* statusData);

    bool commandAvailable();

    int processMessage(byte& commandCode, uint16_t& releaseGap, StatusData * statusData);

    bool isUplinkConnected();

private:

    byte          _mac[6];
    IPAddress     _ip;
    IPAddress     _remoteUDPIp;
    unsigned int  _localUDPPort;
    unsigned int  _remoteUDPPort;
    unsigned int  _localTCPPort;

    char _packetBuffer[UDP_TX_PACKET_MAX_SIZE];

    EthernetUDP _udp;
    EthernetServer* _tcpServer;
    EthernetClient _client;

    bool _isUDPConnected;

    int createHeader(byte *header, TypeMessage type);
    int addChecksum(byte *message, unsigned int length);

    int sendData(const byte data[], int length);

    uint16_t      _n_message;
    byte _lastReadBytes[(HEADER_SECTION_LENGTH + COMMAND_DATA_SECTION_LENGTH)];
    int _numBytesReceived;

    time_t _timeLastMessageReceived;

};

#endif // Communication_lib_h
