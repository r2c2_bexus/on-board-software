//
// Created by guillermo on 02/07/19.
//

#ifndef R2C2_COMMUNICATION_PROTOCOL_H
#define R2C2_COMMUNICATION_PROTOCOL_H

// HEADER

#define HEADER_SECTION_LENGTH      7

#define SENDING_ADDRESS_BIT        7
#define TYPE_MESSAGE_FIRST_BIT     5

enum SendingAddress { GROUND = 0, MIND = 1};
enum TypeMessage { SENSOR_DATA = 0, STATUS_DATA = 1, COMMAND = 2, PING = 3};

#define CHECKSUM_BITS              5

// Sensor Data Transmission: Data Section

#define SENSOR_DATA_SECTION_LENGTH 34

// Status Data Transmission: Status Section

#define STATUS_DATA_SECTION_LENGTH 11

// Command Data

#define COMMAND_DATA_SECTION_LENGTH 3

#define CMD_PASSIVE_MODE               0x00
#define CMD_ACTIVE_MODE                0x01
#define CMD_MANUAL_MODE                0x02
#define CMD_AUTONOMOUS_MODE            0x03

#define CMD_RELEASE_CONTAINER_1        0x04
#define CMD_RELEASE_CONTAINER_2        0x05
#define CMD_RELEASE_CONTAINER_3        0x06

#define CMD_ACTIVATE_MOTOR_1           0x07
#define CMD_DEACTIVATE_MOTOR_1         0x08
#define CMD_ACTIVATE_MOTOR_2           0x09
#define CMD_DEACTIVATE_MOTOR_2         0x0A

#define CMD_ACTIVATE_HEATER_1          0x0B
#define CMD_DEACTIVATE_HEATER_1        0x0C
#define CMD_ACTIVATE_HEATER_2          0x0D
#define CMD_DEACTIVATE_HEATER_2        0x0E
#define CMD_ACTIVATE_HEATER_3          0x0F
#define CMD_DEACTIVATE_HEATER_3        0x10
#define CMD_ACTIVATE_HEATER_4          0x11
#define CMD_DEACTIVATE_HEATER_4        0x12
#define CMD_ACTIVATE_HEATER_5          0x13
#define CMD_DEACTIVATE_HEATER_5        0x14

#define CMD_CHANGE_RELEASE_GAP_TIME    0x15
#define CMD_CAMERA_MANAGER_ON          0x16
#define CMD_CAMERA_MANAGER_OFF         0x17

#define CMD_RETRACT_SERVO_1            0x18
#define CMD_RETRACT_SERVO_2            0x19
#define CMD_RETRACT_SERVO_3            0x1A

#define CMD_CAMERA_MANAGER_REC         0x1B
#define CMD_CAMERA_MANAGER_STOP        0x1C

#define CMD_RESET_I2C_BUS              0x1D
#define CMD_SET_LAUNCH_TIME            0x1E

#define PING                           0xFF


// Ping

#define TIMEOUT_DESCONECTION           300  // seconds


#endif //R2C2_COMMUNICATION_PROTOCOL_H