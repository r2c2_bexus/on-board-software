/*
  r2c2_data.h - Representation of the sensor and status data on board of R2C2 BEXUS experiment.
  Created by Guillermo Zaragoza Prous, May 6, 2019.
  Released into the public domain.
*/

#include "r2c2_data.h"


/*
 *-----------------------
 *	SENSOR DATA	-
 *-----------------------
 */

SensorData::SensorData()
{
	for(int i = 0; i < NUM_TEMPERATURES; i++)
	{
		_temperatures[i] = -127.0;
	}

	_humidity = 0.0;
	_longitude = -1.0;
	_latitude = -1.0;
	_altitude = 0.0;
	_pressure = 0.0;

    _container1Detected = false;
    _container2Detected = false;
    _container3Detected = false;

    _accX = 0.0;
    _accY = 0.0;
    _accZ = 0.0;
    _magX = 0.0;
    _magY = 0.0;
    _magZ = 0.0;
    _gyrX = 0.0;
    _gyrY = 0.0;
    _gyrZ = 0.0;

    _encoder1Pulses = 0;
    _encoder2Pulses = 0;
}


float SensorData::getTemperature(unsigned int numTemperature) const
{
	if (numTemperature <= NUM_TEMPERATURES)
	{
		return _temperatures[numTemperature - 1];
	}
	return -127.0;
}

float SensorData::getHumidity() const
{
	return _humidity;	
}

float SensorData::getLongitude() const
{
	return _longitude;
}

float SensorData::getLatitude() const
{
	return _latitude;
}

uint16_t SensorData::getAltitude() const
{
	return _altitude;
}

bool SensorData::getContainer1Detected() const
{
    return _container1Detected;
}

bool SensorData::getContainer2Detected() const
{
    return _container2Detected;
}

bool SensorData::getContainer3Detected() const
{
    return _container3Detected;
}

uint16_t SensorData::getPressure() const
{
	return _pressure;
}

float SensorData::getAccX() const
{
    return _accX;
}

float SensorData::getAccY() const
{
    return _accY;
}

float SensorData::getAccZ() const
{
    return _accZ;
}

float SensorData::getMagX() const
{
    return _magX;
}

float SensorData::getMagY() const
{
    return _magY;
}

float SensorData::getMagZ() const
{
    return _magZ;
}

float SensorData::getGyrX() const
{
    return _gyrX;
}

float SensorData::getGyrY() const
{
    return _gyrY;
}

float SensorData::getGyrZ() const
{
    return _gyrZ;
}

uint32_t SensorData::getEncoder1Pulses() const
{
    return _encoder1Pulses;
}

uint32_t SensorData::getEncoder2Pulses() const
{
    return _encoder2Pulses;
}

void SensorData::setTemperature(unsigned int numTemperature, float value)
{
	if (numTemperature <= NUM_TEMPERATURES) 
	{
		_temperatures[numTemperature - 1] = value;
	}
}

void SensorData::setHumidity(float value){
	_humidity = value;
}

void SensorData::setLongitude(float value)
{
	_longitude = value;
}

void SensorData::setLatitude(float value)
{
	_latitude = value;
}

void SensorData::setAltitude(uint16_t value)
{
	_altitude = value;
}

void SensorData::setPressure(uint16_t value)
{
	_pressure = value;
}

void SensorData::setContainer1Detected(bool value)
{
    _container1Detected = value;
}

void SensorData::setContainer2Detected(bool value)
{
    _container2Detected = value;
}

void SensorData::setContainer3Detected(bool value)
{
    _container3Detected = value;
}

void SensorData::setAccX(float value)
{
    _accX = value;
}

void SensorData::setAccY(float value)
{
    _accY = value;
}

void SensorData::setAccZ(float value)
{
    _accZ = value;
}

void SensorData::setMagX(float value)
{
    _magX = value;
}

void SensorData::setMagY(float value)
{
    _magY = value;
}

void SensorData::setMagZ(float value)
{
    _magZ = value;
}

void SensorData::setGyrX(float value)
{
    _gyrX = value;
}

void SensorData::setGyrY(float value)
{
    _gyrY = value;
}

void SensorData::setGyrZ(float value)
{
    _gyrZ = value;
}

void SensorData::setEncoder1Pulses(uint32_t value)
{
    _encoder1Pulses = value;
}

void SensorData::setEncoder2Pulses(uint32_t value)
{
    _encoder2Pulses = value;
}

/*
 *-----------------------
 *	STATUS DATA	-
 *-----------------------
 */


StatusData::StatusData()
{

	_launch_time = now();

	_experiment_st = EXP_OK;

	for(int i = 0; i < NUM_CONTAINERS; i++)
	{
		_containers_st[i] = CONT_OK;
		_container_release_time[i] = _launch_time;
	}

	for(int i = 0; i < NUM_TEMPERATURE_SENSORS; i++)
	{
		_temperatures_st[i] = SENS_OK;
	}

	_humidity_st = SENS_OK;
	_gps_st = SENS_OK;
	_pressure_st = SENS_OK;
    _imu_st = SENS_OK;

	_state_machine_st = MANUAL;
	_connection       = COM_DISCONNECTED;

	_camera_st = CAM_OFF;

	for(int i = 0; i < NUM_HEATERS; i++)
	{
		_heaters_st[i] = ACT_OFF;
	}

    for(int i = 0; i < NUM_MOTORS; i++)
    {
        _motors_st[i] = ACT_OFF;
    }
	
	for(int i = 0; i < NUM_SERVOS; i++)
	{
		_servos_st[i] = ACT_OFF;
	}
	
	_release_gap_time = DEFAULT_INITIAL_RELEASE_GAP_TIME;
	_releasing = false;

}

Experiment_Status StatusData::getExperimentStatus() const
{
	return _experiment_st;
}

Container_Status StatusData::getContainerStatus(unsigned int numContainer) const
{
	if(numContainer <= NUM_CONTAINERS)
	{
		return _containers_st[numContainer - 1];
	}
	return CONT_ERROR;
}

Sensor_Status StatusData::getTemperatureSensorStatus(unsigned int numSensor) const
{
	if(numSensor <= NUM_TEMPERATURE_SENSORS)
	{
		return _temperatures_st[numSensor - 1];
	}
	return SENS_ERROR;
}

Sensor_Status StatusData::getHumiditySensorStatus() const
{
	return _humidity_st;
}

Sensor_Status StatusData::getGPSStatus() const
{
	return _gps_st;
}

Sensor_Status StatusData::getPressureStatus() const
{
	return _pressure_st;
}

Sensor_Status StatusData::getIMUStatus() const
{
    return _imu_st;
}

State_Machine_Status StatusData::getStateMachineStatus() const
{
	return _state_machine_st;
}

Communication_Status StatusData::getCommunicationStatus() const
{
    return _connection;
}

bool StatusData::isReleasing() const
{
    return _releasing;
}

Camera_Status StatusData::getCameraStatus() const
{
	return _camera_st;
}

Actuator_Status StatusData::getHeaterStatus(unsigned int numHeater) const
{
	if (numHeater <= NUM_HEATERS)
	{
		return _heaters_st[numHeater - 1];
	}
	return ACT_OFF;
}

Actuator_Status StatusData::getMotorStatus(unsigned int numMotor) const
{
    if (numMotor <= NUM_MOTORS)
    {
        return _motors_st[numMotor - 1];
    }
    return ACT_OFF;
}

Actuator_Status StatusData::getServoStatus(unsigned int numServo) const
{
	if (numServo <= NUM_SERVOS)
	{
		return _servos_st[numServo - 1];
	}
	return ACT_OFF;
}

time_t StatusData::getLaunchTime() const
{
	return _launch_time;
}

unsigned int StatusData::getReleaseGapTime() const
{
	return _release_gap_time;
}

time_t StatusData::getContainerReleaseTime(unsigned int numContainer) const
{
	if(numContainer <= NUM_CONTAINERS)
	{
		return _container_release_time[numContainer - 1];
	}

	return _launch_time;
}

unsigned int StatusData::getLastContainerReleased() const
{
    unsigned int lastContainerReleased = 0;
    time_t timeLastRelease = _launch_time;

    for (int i = 1; i <= NUM_CONTAINERS; i++) {
        int diff_with_launch_time = _container_release_time[i - 1] - _launch_time;
        int diff_with_last_release_time = _container_release_time[i - 1] - timeLastRelease;

        if (diff_with_launch_time > 0 && diff_with_last_release_time > 0)
        {
            timeLastRelease = _container_release_time[i - 1];
            lastContainerReleased = i;
        }
    }
    return lastContainerReleased;
}

time_t StatusData::getLastContainerReleasedTime() const
{
    time_t lastContainerReleasedTime = _launch_time;
    unsigned int index = getLastContainerReleased();
    if (index > 0)
    {
        lastContainerReleasedTime = _container_release_time[index - 1];
    }
    return lastContainerReleasedTime;
}

void StatusData::setExperimentStatus(Experiment_Status value)
{
	if(EXP_OK <= value && value <= EXP_WARNING)
	{
		_experiment_st = value;
	}
}

void StatusData::setContainerStatus(unsigned int numContainer, Container_Status value)
{
	if(CONT_OK <= value && value <= CONT_RELEASED && numContainer <= NUM_CONTAINERS)
	{
		_containers_st[numContainer - 1] = value;
	}
}

void StatusData::setTemperatureSensorStatus(unsigned int numSensor, Sensor_Status value)
{
	if(SENS_OK <= value && value <= SENS_ERROR && numSensor <= NUM_TEMPERATURE_SENSORS)
	{
		_temperatures_st[numSensor - 1] = value;
	}
}

void StatusData::setHumiditySensorStatus(Sensor_Status value)
{
	if(SENS_OK <= value && value <= SENS_ERROR)
	{
		_humidity_st = value;
	}
}

void StatusData::setGPSStatus(Sensor_Status value)
{
	if(SENS_OK <= value && value <= SENS_ERROR)
	{
		_gps_st = value;
	}
}

void StatusData::setPressureStatus(Sensor_Status value)
{
	if(SENS_OK <= value && value <= SENS_ERROR)
	{
		_pressure_st = value;
	}
}

void StatusData::setIMUStatus(Sensor_Status value)
{
    if(SENS_OK <= value && value <= SENS_ERROR)
    {
        _imu_st = value;
    }
}

void StatusData::setStateMachineStatus(State_Machine_Status value)
{
	if(PASSIVE <= value && value <= AUTONOMOUS)
	{
		_state_machine_st = value;
	}
}

void StatusData::setCommunicationStatus(Communication_Status value)
{
    if (COM_CONNECTED <= value && value <= COM_DISCONNECTED)
    {
        _connection = value;
    }
}

void StatusData::setReleasingStatus(bool value)
{
    _releasing = value;
}

void StatusData::setCameraStatus(Camera_Status value)
{
	if(CAM_ON <= value && value <= CAM_RECORDING)
	{
		_camera_st = value;
	}
}

void StatusData::setHeaterStatus(unsigned int numHeater, Actuator_Status value)
{
	if(ACT_ON <= value && value <= ACT_OFF && numHeater <= NUM_HEATERS)
	{
		_heaters_st[numHeater - 1] = value;
	}
}

void StatusData::setMotorStatus(unsigned int numMotor, Actuator_Status value)
{
	if(ACT_ON <= value && value <= ACT_OFF)
	{
		_motors_st[numMotor - 1] = value;
	}
}

void StatusData::setServoStatus(unsigned int numServo, Actuator_Status value)
{
	if(ACT_ON <= value && value <= ACT_OFF && numServo <= NUM_SERVOS)
	{
		_servos_st[numServo - 1] = value;
	}
}

void StatusData::setLaunchTime(time_t value)
{
	_launch_time = value;
}

void StatusData::setReleaseGapTime(unsigned int value)
{
	_release_gap_time = value;
}

void StatusData::setContainerReleaseTime(unsigned int numContainer, time_t value)
{
	if(numContainer <= NUM_CONTAINERS)
	{
		_container_release_time[numContainer - 1] = value;
	}
}

void StatusData::resetExperimentTime(unsigned int hr, unsigned int min, unsigned int sec, bool launchToNow)
{
    if (!launchToNow) {
        time_t currentTimeBeforeSet = now();
        unsigned int secondsAfterLaunch = hour(currentTimeBeforeSet) * 3600 + minute(currentTimeBeforeSet) * 60 +
                                          second(currentTimeBeforeSet);

        int launchSeconds = sec - (secondsAfterLaunch % 60);
        secondsAfterLaunch -= (secondsAfterLaunch % 60);

        int launchMinutes = min - (secondsAfterLaunch / 60);
        secondsAfterLaunch -= (secondsAfterLaunch / 60);

        int launchHours = hr - (secondsAfterLaunch / 3600);
        secondsAfterLaunch -= (secondsAfterLaunch / 3600);

        int launchDay = DAY;

        if (launchSeconds < 0) {
            launchSeconds = 60 + launchSeconds;
            launchMinutes--;
        }

        if (launchMinutes < 0) {
            launchMinutes = 60 + launchMinutes;
            launchHours--;
        }

        if (launchHours < 0) {
            launchHours = 24 + launchHours;
            launchDay--;
        }

        setTime(launchHours, launchMinutes, launchSeconds, launchDay, MONTH, YEAR);
        time_t launchTime = now();
        setLaunchTime(launchTime);

        time_t timeOfRelease1 = getContainerReleaseTime(1);
        setContainerReleaseTime(1, launchTime + timeOfRelease1);

        time_t timeOfRelease2 = getContainerReleaseTime(2);
        setContainerReleaseTime(2, launchTime + timeOfRelease2);

        time_t timeOfRelease3 = getContainerReleaseTime(3);
        setContainerReleaseTime(3, launchTime + timeOfRelease3);

        setTime(hr, min, sec, DAY, MONTH, YEAR);
        Serial.println("Time set to: " + String(hr) + ":" + String(min) + ":" + String(sec));
    }
    else
    {
        setTime(hr, min, sec, DAY, MONTH, YEAR);
        time_t launchTime = now();
        setLaunchTime(launchTime);
        setContainerReleaseTime(1, launchTime);
        setContainerReleaseTime(2, launchTime);
        setContainerReleaseTime(3, launchTime);

        Serial.println("Time set to: " + String(hr) + ":" + String(min) + ":" + String(sec));
    }

}



String stateMachineToString(State_Machine_Status status)
{
    String stateMachineString;

    switch (status)
    {
        case PASSIVE:
            stateMachineString = "PASSIVE";
            break;
        case MANUAL:
            stateMachineString = "MANUAL";
            break;
        case AUTONOMOUS:
            stateMachineString = "AUTONOMOUS";
            break;
    }
    return stateMachineString;
}
