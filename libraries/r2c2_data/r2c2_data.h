/*
  r2c2_data.cpp - Representation of the sensor and status data on board of R2C2 BEXUS experiment.
  Created by Guillermo Zaragoza Prous, May 6, 2019.
  Released into the public domain.
*/

#ifndef Data_lib_h
#define Data_lib_h

#include <stdint.h>
#include <TimeLib.h>
#include <Arduino.h>


#define YEAR                    2019
#define MONTH                   10
#define DAY                     24

#define NUM_TEMPERATURES	    6

#define NUM_TEMPERATURE_SENSORS	6
#define NUM_CONTAINERS			3
#define NUM_HEATERS				5
#define NUM_SERVOS				3
#define NUM_MOTORS              2

#define TEMPERATURE_SENS_1_ID   1
#define TEMPERATURE_SENS_2_ID   2
#define TEMPERATURE_SENS_3_ID   3
#define TEMPERATURE_SENS_4_ID   4
#define TEMPERATURE_SENS_5_ID   5
#define TEMPERATURE_SENS_6_ID   6
#define HUMIDITY_SENS_ID        7
#define GPS_ID                  8
#define PRESSURE_SENS_ID        9
#define IMU_SENS_ID            10

#define DEFAULT_INITIAL_RELEASE_GAP_TIME	4000 // seconds
#define DEFAULT_RELEASE_GAP_TIME            1800 // seconds

#define CONT_DETECTED           0
#define CONT_NOT_DETECTED       1

enum Experiment_Status 		 {EXP_OK = 0, EXP_ERROR, EXP_WARNING};
enum Container_Status 		 {CONT_OK = 0, CONT_ERROR, CONT_RELEASED};
enum Sensor_Status 		     {SENS_OK = 0, SENS_ERROR};
enum State_Machine_Status 	 {PASSIVE = 0, MANUAL, AUTONOMOUS};
enum Actuator_Status 		 {ACT_ON = 0, ACT_OFF};
enum Camera_Status 		     {CAM_ON = 0, CAM_OFF, CAM_RECORDING};
enum Communication_Status    {COM_CONNECTED = 0, COM_DISCONNECTED};

/* *************************
 * Name: SensorData class
 * Description: This class is a representation of the data that is going to be 
 * acquired from several sensors during the flight time that the BEXUS 29 
 * balloon will be in the air. These measurements consists of: temperatures, 
 * humidity, pressure and position (latitude, longitude and altitude).
 *
 * Considerations of usage:
 * The initial values (and units) for the data are:
 * - Temperatures: -127 celsius degrees
 * - Humidity: 0 percent
 * - Pressure: 0 millibar
 * - Latitude: -1.0 decimal degrees
 * - Longitude: -1.0 decimal degrees
 * - Altitude: 0 meters
*/

class SensorData
{
	public:
		SensorData();
        float		getTemperature(unsigned int numTemperature) const;
        float		getHumidity() const;
		float		getLongitude() const;
		float		getLatitude() const;
        uint16_t	getAltitude() const;
        uint16_t	getPressure() const;

        bool        getContainer1Detected() const;
        bool        getContainer2Detected() const;
        bool        getContainer3Detected() const;

        float       getAccX() const;
        float       getAccY() const;
        float       getAccZ() const;
        float       getMagX() const;
        float       getMagY() const;
        float       getMagZ() const;
        float       getGyrX() const;
        float       getGyrY() const;
        float       getGyrZ() const;

        uint32_t getEncoder1Pulses() const;
        uint32_t getEncoder2Pulses() const;

		void		setTemperature(unsigned int numTemperature, float value);
		void		setHumidity(float value);
		void		setLongitude(float value);
		void		setLatitude(float value);
		void		setAltitude(uint16_t value);
		void		setPressure(uint16_t value);

		void		setContainer1Detected(bool value);
		void		setContainer2Detected(bool value);
		void		setContainer3Detected(bool value);

		void        setAccX(float value);
		void        setAccY(float value);
		void        setAccZ(float value);
        void        setMagX(float value);
        void        setMagY(float value);
        void        setMagZ(float value);
        void        setGyrX(float value);
        void        setGyrY(float value);
        void        setGyrZ(float value);

        void        setEncoder1Pulses(uint32_t value);
        void        setEncoder2Pulses(uint32_t value);

	private:
        float _temperatures[NUM_TEMPERATURES];
        float _humidity;
		float   _longitude;
		float   _latitude;
        uint16_t _altitude;
        uint16_t _pressure;

        bool _container1Detected;
        bool _container2Detected;
        bool _container3Detected;

        float _accX;
        float _accY;
        float _accZ;
        float _magX;
        float _magY;
        float _magZ;
        float _gyrX;
        float _gyrY;
        float _gyrZ;

        uint32_t _encoder1Pulses;
        uint32_t _encoder2Pulses;
};


/* ************************
 * Name: StatusData class
 * Description: This class is a representation of the statuses that different
 * parts of the system have in a certain moment. These statuses include the
 * general status of the experiment, the state machine, the statuses of each
 * of the sensors and actuators and the statuses of the containers.
 * 
 * Considerations of usage:
 * The initial values for each of the statuses is X_UNINITIALIZED, with X 
 * being the type of ENUM that corresponds to each of the statuses. The
 * launch time is initialized at the current time when the new object is created,
 * as well as the containers release time.
*/

class StatusData
{

	public:
		StatusData();

		// Returns the status of the experiment.
		Experiment_Status 	getExperimentStatus() const;

		// Returns the status of the container. Argument between 1 and N.
		Container_Status  	getContainerStatus(unsigned int numContainer) const;

		Sensor_Status     	getTemperatureSensorStatus(unsigned int numSensor) const;
		Sensor_Status	  	getHumiditySensorStatus() const;
		Sensor_Status     	getGPSStatus() const;
		Sensor_Status     	getPressureStatus() const;
        Sensor_Status     	getIMUStatus() const;

		State_Machine_Status	getStateMachineStatus() const;
        Communication_Status    getCommunicationStatus() const;
        bool                    isReleasing() const;

        Camera_Status		getCameraStatus() const;
		Actuator_Status		getHeaterStatus(unsigned int numHeater) const;
		Actuator_Status		getMotorStatus(unsigned int numMotor) const;
		Actuator_Status		getServoStatus(unsigned int numServo) const;

		time_t			getLaunchTime() const;
		unsigned int	getReleaseGapTime() const;
		time_t			getContainerReleaseTime(unsigned int numContainer) const;
		unsigned int    getLastContainerReleased() const;
        time_t          getLastContainerReleasedTime() const;

		void 			setExperimentStatus(Experiment_Status value);

		void 			setContainerStatus(unsigned int numContainer, Container_Status value);

		void 			setTemperatureSensorStatus(unsigned int numSensor, Sensor_Status value);
		void 			setHumiditySensorStatus(Sensor_Status value);
		void 			setGPSStatus(Sensor_Status value);
		void 			setPressureStatus(Sensor_Status value);
		void 			setIMUStatus(Sensor_Status value);

		void 			setStateMachineStatus(State_Machine_Status value);
		void            setCommunicationStatus(Communication_Status value);
		void            setReleasingStatus(bool value);

		void 			setCameraStatus(Camera_Status value);
		void 			setHeaterStatus(unsigned int numHeater, Actuator_Status value); 
		void 			setMotorStatus(unsigned int numMotor, Actuator_Status value);
		void 			setServoStatus(unsigned int numServo, Actuator_Status value);

		void 			setLaunchTime(time_t value);
		void 			setReleaseGapTime(unsigned int value);
		void 			setContainerReleaseTime(unsigned int numContainer, time_t value);

		void            resetExperimentTime(unsigned int hr, unsigned int min, unsigned int sec, bool launchToNow = false);

	private:
		Experiment_Status 	_experiment_st;
		Container_Status  	_containers_st[NUM_CONTAINERS];
        Communication_Status _connection;
        bool                _releasing;
		
		Sensor_Status     	_temperatures_st[NUM_TEMPERATURE_SENSORS];
		Sensor_Status     	_humidity_st;
		Sensor_Status     	_gps_st;
		Sensor_Status     	_pressure_st;
		Sensor_Status       _imu_st;

		State_Machine_Status	_state_machine_st;
		
		Camera_Status       _camera_st;
		Actuator_Status		_heaters_st[NUM_HEATERS];
		Actuator_Status		_motors_st[NUM_MOTORS];
		Actuator_Status		_servos_st[NUM_SERVOS];
		
		time_t			_launch_time;
		unsigned int	_release_gap_time;
		time_t			_container_release_time[NUM_CONTAINERS];

};

String stateMachineToString(State_Machine_Status status);


#endif // Data_lib_h
