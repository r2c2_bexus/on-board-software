
#include "r2c2_safety_lib.h"
#include <TimeLib.h>


enum Releasing_State         {NO_RELEASE, PRE_RELEASE, RELEASE, REEL_UP, POST_REEL_UP, POST_RELEASE};

Safety::Safety(Store *store): _store(store)
{

}

SafetyImpl::SafetyImpl(Store *store): Safety(store)
{

}

bool SafetyImpl::rule1CheckAltitudeForRelease(const SensorData *sensorData) const
{
    return sensorData->getAltitude() > MIN_ALTITUDE;
}

bool SafetyImpl::rule2CheckReleaseTimeGap(const StatusData *statusData) const
{
    unsigned int releaseTimeGap = statusData->getReleaseGapTime();

    time_t timeLastContainer = statusData->getLastContainerReleasedTime();
    time_t timeNow = now();

    return ((timeNow - timeLastContainer) > releaseTimeGap);
}

bool SafetyImpl::rule3AMotor1HeaterFunctioning(const SensorData *sensorData) const
{
    static const unsigned int temp_margin = 15;
    float temp1 = sensorData->getTemperature(TEMPERATURE_SENSOR_MOTOR1);

    return (temp1 > (MIN_TEMPERATURE - temp_margin));
}

bool SafetyImpl::rule3BMotor2HeaterFunctioning(const SensorData *sensorData) const
{
    static const unsigned int temp_margin = 15;
    float temp2 = sensorData->getTemperature(TEMPERATURE_SENSOR_MOTOR2);

    return (temp2 > (MIN_TEMPERATURE - temp_margin));
}

bool SafetyImpl::checkPreReleaseSafetyRules(const SensorData *sensorData, const StatusData *statusData) const
{
    bool isSafe = true;
    if (statusData->getGPSStatus() == SENS_OK)
    {
        isSafe = rule1CheckAltitudeForRelease(sensorData);
    }
    isSafe = isSafe && rule2CheckReleaseTimeGap(statusData);
    _store->log(INFO_LEVEL, "Check pre release safety values. Is Safe? " + String(isSafe ? "true" : "false"));

    return isSafe;
}

bool SafetyImpl::checkCriticalSafetyRules(const SensorData *sensorData, int containerReleased) const
{

    bool isSafe = true;

    switch (containerReleased)
    {
        case 1:
            isSafe = rule3AMotor1HeaterFunctioning(sensorData);
            break;
        case 2:
        case 3:
            isSafe = rule3BMotor2HeaterFunctioning(sensorData);
            break;
    }
    _store->log(INFO_LEVEL, "Check critical safety values. Is Safe? " + String(isSafe ? "true" : "false"));
    return isSafe;
}

bool SafetyImpl::checkIfAscendingPhase(const SensorData* sensorData, const StatusData *statusData) const
{
    bool isSafe = true;
    if (statusData->getGPSStatus() == SENS_OK)
    {
        isSafe = sensorData->getAltitude() < (FLOATING_ALTITUDE - 1000);
    }
    _store->log(INFO_LEVEL, "Check if in ascending phase. Is Safe? " + String(isSafe ? "true" : "false"));

    return isSafe;
}


void SafetyImpl::checkExperimentStatus(StatusData* statusData) const
{
    Experiment_Status experiment_st = EXP_OK;

    // If any sensor in error -> warning
    if (statusData->getTemperatureSensorStatus(1) != SENS_OK ||
        statusData->getTemperatureSensorStatus(2) != SENS_OK ||
        statusData->getTemperatureSensorStatus(3) != SENS_OK ||
        statusData->getTemperatureSensorStatus(4) != SENS_OK ||
        statusData->getTemperatureSensorStatus(5) != SENS_OK ||
        statusData->getTemperatureSensorStatus(6) != SENS_OK ||
        statusData->getHumiditySensorStatus() != SENS_OK ||
        statusData->getGPSStatus() != SENS_OK ||
        statusData->getPressureStatus() != SENS_OK ||
        statusData->getIMUStatus() != SENS_OK )
    {
        experiment_st = EXP_WARNING;
    }

    if (statusData->getCameraStatus() != CAM_OFF)
    {
        experiment_st = EXP_WARNING;
    }

    if (statusData->getHeaterStatus(1) != ACT_OFF ||
        statusData->getHeaterStatus(2) != ACT_OFF ||
        statusData->getHeaterStatus(3) != ACT_OFF ||
        statusData->getHeaterStatus(4) != ACT_OFF ||
        statusData->getHeaterStatus(5) != ACT_OFF ||
        statusData->getMotorStatus(1) != ACT_OFF ||
        statusData->getMotorStatus(2) != ACT_OFF ||
        statusData->getServoStatus(1) != ACT_OFF ||
        statusData->getServoStatus(2) != ACT_OFF ||
        statusData->getServoStatus(3) != ACT_OFF)
    {
        experiment_st = EXP_WARNING;
    }

    if (statusData->getContainerStatus(1) == CONT_ERROR ||
        statusData->getContainerStatus(2) == CONT_ERROR ||
        statusData->getContainerStatus(3) == CONT_ERROR)
    {
        experiment_st = EXP_ERROR;
    }

    statusData->setExperimentStatus(experiment_st);
}

void SafetyImpl::getValueAndCheckSensorStatuses(Sensor* sensor, SensorData* sensorData, StatusData* statusData) const
{

    // Get Sensor data
    float temp1 = sensor->getTemperature(TempSensorAddr::TEMP_1);
    float temp2 = sensor->getTemperature(TempSensorAddr::TEMP_2);
    float temp3 = sensor->getTemperature(TempSensorAddr::TEMP_3);
    float temp4 = sensor->getTemperature(TempSensorAddr::TEMP_4);
    float temp5 = sensor->getTemperature(TempSensorAddr::TEMP_5);
    float temp6 = sensor->getThermistorTemperature();

    sensorData->setTemperature(1, temp1);
    sensorData->setTemperature(2, temp2);
    sensorData->setTemperature(3, temp3);
    sensorData->setTemperature(4, temp4);
    sensorData->setTemperature(5, temp5);
    sensorData->setTemperature(6, temp6);

    Sensor::Position currentPosition = sensor->getPosition();

    if (currentPosition.validity)
    {
        sensorData->setLongitude(currentPosition.longitude);
        sensorData->setLatitude(currentPosition.latitude);
        sensorData->setAltitude(currentPosition.altitude);
    }

    float humidity = sensor->getHumidity();
    sensorData->setHumidity(humidity);

    unsigned int pressure = sensor->getPressure();
    sensorData->setPressure(pressure);

    unsigned int cont1state = sensor->getContainerState(1);
    if (cont1state == 0)
    {
        sensorData->setContainer1Detected(true);
    }
    else if (cont1state == 1)
    {
        sensorData->setContainer1Detected(false);
    }

    unsigned int cont2state = sensor->getContainerState(2);
    if (cont2state == 0)
    {
        sensorData->setContainer2Detected(true);
    }
    else if (cont2state == 1)
    {
        sensorData->setContainer2Detected(false);
    }

    unsigned int cont3state = sensor->getContainerState(3);
    if (cont3state == 0)
    {
        sensorData->setContainer3Detected(true);
    }
    else if (cont3state == 1)
    {
        sensorData->setContainer3Detected(false);
    }

    if(statusData->getCameraStatus() == CAM_RECORDING)
    {
        Sensor::Inclination currentInclination = sensor->getInclination();
        if (currentInclination.validity)
        {
            sensorData->setAccX(currentInclination.accX);
            sensorData->setAccY(currentInclination.accY);
            sensorData->setAccZ(currentInclination.accZ);
            sensorData->setMagX(currentInclination.magX);
            sensorData->setMagY(currentInclination.magY);
            sensorData->setMagZ(currentInclination.magZ);
            sensorData->setGyrX(currentInclination.gyrX);
            sensorData->setGyrY(currentInclination.gyrY);
            sensorData->setGyrZ(currentInclination.gyrZ);

            statusData->setIMUStatus(SENS_OK);
        }
        else
        {
            statusData->setIMUStatus(SENS_ERROR);
        }
    }


    // Encoders
    sensorData->setEncoder1Pulses(sensor->getEncoderState(EncoderSensorAddr::ENCODER_1));
    sensorData->setEncoder2Pulses(sensor->getEncoderState(EncoderSensorAddr::ENCODER_2));

    // Check sensor statuses
    // Temperatures

    static const float min_temp_range = -100.0;
    static const float max_temp_range = 60.0;
    static const unsigned int num_max_temp_fails_until_error = 150;

    static unsigned int temp1ErrorCounter = 0;
    static float temp1LastValue = 0.0;
    if (temp1 == temp1LastValue)
    {
        temp1ErrorCounter++;
    }
    else
    {
        temp1ErrorCounter = 0;
    }
    temp1LastValue = temp1;

    if (temp1 < min_temp_range || temp1 > max_temp_range)
    {
        statusData->setTemperatureSensorStatus(1, SENS_ERROR);
    }
    else if (temp1ErrorCounter >= num_max_temp_fails_until_error)
    {
        statusData->setTemperatureSensorStatus(1, SENS_ERROR);
    }
    else
    {
        statusData->setTemperatureSensorStatus(1, SENS_OK);
    }

    static unsigned int temp2ErrorCounter = 0;
    static float temp2LastValue = 0.0;
    if (temp2 == temp2LastValue)
    {
        temp2ErrorCounter++;
    }
    else
    {
        temp2ErrorCounter = 0;
    }
    temp2LastValue = temp2;

    if (temp2 < min_temp_range || temp2 > max_temp_range)
    {
        statusData->setTemperatureSensorStatus(2, SENS_ERROR);
    }
    else if (temp2ErrorCounter >= num_max_temp_fails_until_error)
    {
        statusData->setTemperatureSensorStatus(2, SENS_ERROR);
    }
    else
    {
        statusData->setTemperatureSensorStatus(2, SENS_OK);
    }

    static unsigned int temp3ErrorCounter = 0;
    static float temp3LastValue = 0.0;
    if (temp3 == temp3LastValue)
    {
        temp3ErrorCounter++;
    }
    else
    {
        temp3ErrorCounter = 0;
    }
    temp3LastValue = temp3;

    if (temp3 < min_temp_range || temp3 > max_temp_range)
    {
        statusData->setTemperatureSensorStatus(3, SENS_ERROR);
    }
    else if (temp3ErrorCounter >= num_max_temp_fails_until_error)
    {
        statusData->setTemperatureSensorStatus(3, SENS_ERROR);
    }
    else
    {
        statusData->setTemperatureSensorStatus(3, SENS_OK);
    }

    static unsigned int temp4ErrorCounter = 0;
    static float temp4LastValue = 0.0;
    if (temp4 == temp4LastValue)
    {
        temp4ErrorCounter++;
    }
    else
    {
        temp4ErrorCounter = 0;
    }
    temp4LastValue = temp4;

    if (temp4 < min_temp_range || temp4 > max_temp_range)
    {
        statusData->setTemperatureSensorStatus(4, SENS_ERROR);
    }
    else if (temp4ErrorCounter >= num_max_temp_fails_until_error)
    {
        statusData->setTemperatureSensorStatus(4, SENS_ERROR);
    }
    else
    {
        statusData->setTemperatureSensorStatus(4, SENS_OK);
    }

    static unsigned int temp5ErrorCounter = 0;
    static float temp5LastValue = 0.0;
    if (temp5 == temp5LastValue)
    {
        temp5ErrorCounter++;
    }
    else
    {
        temp5ErrorCounter = 0;
    }
    temp5LastValue = temp5;

    if (temp5 < min_temp_range || temp5 > max_temp_range)
    {
        statusData->setTemperatureSensorStatus(5, SENS_ERROR);
    }
    else if (temp5ErrorCounter >= num_max_temp_fails_until_error)
    {
        statusData->setTemperatureSensorStatus(5, SENS_ERROR);
    }
    else
    {
        statusData->setTemperatureSensorStatus(5, SENS_OK);
    }

    static unsigned int temp6ErrorCounter = 0;
    static float temp6LastValue = 0.0;
    if (temp6 == temp6LastValue)
    {
        temp6ErrorCounter++;
    }
    else
    {
        temp6ErrorCounter = 0;
    }
    temp6LastValue = temp6;

    if (temp6 < min_temp_range || temp6 > max_temp_range)
    {
        statusData->setTemperatureSensorStatus(6, SENS_ERROR);
    }
    else if (temp6ErrorCounter >= num_max_temp_fails_until_error)
    {
        statusData->setTemperatureSensorStatus(6, SENS_ERROR);
    }
    else
    {
        statusData->setTemperatureSensorStatus(6, SENS_OK);
    }

    // Humidity

    static const float min_humid_range = 0.0;
    static const float max_humid_range = 100.0;
    static const float humid_error_value = 0.0000;

    static unsigned int humidErrorCounter = 0;
    if (humidity == humid_error_value)
    {
        humidErrorCounter++;
    }
    else
    {
        humidErrorCounter = 0;
    }

    if (humidity < min_humid_range || humidity > max_humid_range)
    {
        statusData->setHumiditySensorStatus(SENS_ERROR);
    }
    else if (humidErrorCounter >= 50)
    {
        statusData->setHumiditySensorStatus(SENS_ERROR);
    }
    else
    {
        statusData->setHumiditySensorStatus(SENS_OK);
    }

    // GPS

    static unsigned int gpsErrorCounter = 0;

    if (!currentPosition.validity)
    {
        gpsErrorCounter++;
    }
    else
    {
        gpsErrorCounter = 0;
    }

    if (gpsErrorCounter < 50)
    {
        statusData->setGPSStatus(SENS_OK);
    }
    else
    {
        statusData->setGPSStatus(SENS_ERROR);
    }

    // Pressure

    static const unsigned int min_pressure_range = 0;
    static const unsigned int max_pressure_range = 1100;
    static const unsigned int pressure_error_value = 0;

    static unsigned int pressureErrorCounter = 0;
    if (pressure == pressure_error_value)
    {
        pressureErrorCounter++;
    }
    else
    {
        pressureErrorCounter = 0;
    }

    if (pressure < min_pressure_range || pressure > max_pressure_range)
    {
        statusData->setPressureStatus(SENS_ERROR);
    }
    else if (pressureErrorCounter >= 50)
    {
        statusData->setPressureStatus(SENS_ERROR);
    }
    else
    {
        statusData->setPressureStatus(SENS_OK);
    }

    // Detector Switch


    bool cont1Detected = sensorData->getContainer1Detected();

    if (cont1Detected && statusData->getContainerStatus(1) != CONT_RELEASED)
    {
        statusData->setContainerStatus(1, CONT_OK);
    }
    else if(!cont1Detected && statusData->getContainerStatus(1) != CONT_RELEASED)
    {
        statusData->setContainerStatus(1, CONT_ERROR);
    }

    bool cont2Detected = sensorData->getContainer2Detected();

    if (cont2Detected && statusData->getContainerStatus(2) != CONT_RELEASED)
    {
        statusData->setContainerStatus(2, CONT_OK);
    }
    else if(!cont2Detected && statusData->getContainerStatus(2) != CONT_RELEASED)
    {
        statusData->setContainerStatus(2, CONT_ERROR);
    }

    bool cont3Detected = sensorData->getContainer3Detected();

    if (cont3Detected && statusData->getContainerStatus(3) != CONT_RELEASED)
    {
        statusData->setContainerStatus(3, CONT_OK);
    }
    else if(!cont3Detected && statusData->getContainerStatus(3) != CONT_RELEASED)
    {
        statusData->setContainerStatus(3, CONT_ERROR);
    }

}

void SafetyImpl::checkActuatorStatuses(Actuator* actuator, Sensor * sensor, StatusData* statusData) const
{

    bool componentStatus = actuator->isMotorOn(MotorAddr::MOTOR_1);
    Actuator_Status actuatorStatus = (componentStatus) ? ACT_ON : ACT_OFF;
    statusData->setMotorStatus(1, actuatorStatus);

    componentStatus = actuator->isMotorOn(MotorAddr::MOTOR_2);
    actuatorStatus = (componentStatus) ? ACT_ON : ACT_OFF;
    statusData->setMotorStatus(2, actuatorStatus);

    componentStatus = actuator->isServoOn(ServoAddr::SERVO_1);
    actuatorStatus = (componentStatus) ? ACT_ON : ACT_OFF;
    statusData->setServoStatus(1, actuatorStatus);

    componentStatus = actuator->isServoOn(ServoAddr::SERVO_2);
    actuatorStatus = (componentStatus) ? ACT_ON : ACT_OFF;
    statusData->setServoStatus(2, actuatorStatus);

    componentStatus = actuator->isServoOn(ServoAddr::SERVO_3);
    actuatorStatus = (componentStatus) ? ACT_ON : ACT_OFF;
    statusData->setServoStatus(3, actuatorStatus);

    componentStatus = actuator->isHeaterOn(HeaterAddr::HEATER_1);
    actuatorStatus = (componentStatus) ? ACT_ON : ACT_OFF;
    statusData->setHeaterStatus(1, actuatorStatus);

    componentStatus = actuator->isHeaterOn(HeaterAddr::HEATER_2);
    actuatorStatus = (componentStatus) ? ACT_ON : ACT_OFF;
    statusData->setHeaterStatus(2, actuatorStatus);

    componentStatus = actuator->isHeaterOn(HeaterAddr::HEATER_3);
    actuatorStatus = (componentStatus) ? ACT_ON : ACT_OFF;
    statusData->setHeaterStatus(3, actuatorStatus);

    componentStatus = actuator->isHeaterOn(HeaterAddr::HEATER_4);
    actuatorStatus = (componentStatus) ? ACT_ON : ACT_OFF;
    statusData->setHeaterStatus(4, actuatorStatus);

    componentStatus = actuator->isHeaterOn(HeaterAddr::HEATER_5);
    actuatorStatus = (componentStatus) ? ACT_ON : ACT_OFF;
    statusData->setHeaterStatus(5, actuatorStatus);

    componentStatus = actuator->isCameraManagerSystemOn();
    bool componentStatus2 = sensor->isCameraRecording();
    Camera_Status cameraStatus = CAM_OFF;
    if(componentStatus && componentStatus2)
    {
        cameraStatus = CAM_RECORDING;
    }
    else if (componentStatus)
    {
        cameraStatus = CAM_ON;
    }
    statusData->setCameraStatus(cameraStatus);
}

Autonomous::Autonomous(Store * store, Actuator * actuator, Sensor * sensor):
_store(store), _actuator(actuator), _sensor(sensor)
{
    for (int i = 1; i <= NUM_TEMPERATURE_SENSORS; i++)
    {
        setMinTemperature(i, MIN_TEMPERATURE);
        setMaxTemperature(i, MAX_TEMPERATURE);
    }
}

float Autonomous::getMinTemperature(unsigned int numSensor) const
{
    float temperature = -100;
    if (1 <= numSensor && numSensor <= NUM_TEMPERATURE_SENSORS)
    {
        temperature = _min_temperatures[numSensor - 1];
    }
    return temperature;
}

void Autonomous::setMinTemperature(unsigned int numSensor, float temperature)
{
    if (1 <= numSensor && numSensor <= NUM_TEMPERATURE_SENSORS)
    {
        _min_temperatures[numSensor - 1] = temperature;
    }
}

float Autonomous::getMaxTemperature(unsigned int numSensor) const
{
    float temperature = 100;
    if (1 <= numSensor && numSensor <= NUM_TEMPERATURE_SENSORS)
    {
        temperature = _max_temperatures[numSensor - 1];
    }
    return temperature;
}

void Autonomous::setMaxTemperature(unsigned int numSensor, float temperature)
{
    if (1 <= numSensor && numSensor <= NUM_TEMPERATURE_SENSORS)
    {
        _max_temperatures[numSensor - 1] = temperature;
    }
}

AutonomousImpl::AutonomousImpl(Store * store, Actuator * actuator, Sensor * sensor): Autonomous(store, actuator, sensor)
{

}

void AutonomousImpl::checkAllTemperatures(SensorData * sensorData, StatusData * statusData) {

    int heatersArray[5] = {HeaterAddr::HEATER_1, HeaterAddr::HEATER_2, HeaterAddr::HEATER_3, HeaterAddr::HEATER_4, HeaterAddr::HEATER_5};

    float temp = -1.0;

    if (statusData->getTemperatureSensorStatus(TEMPERATURE_SENSOR_BOX) == SENS_OK)
    {
        temp = sensorData->getTemperature(TEMPERATURE_SENSOR_BOX);
        temperatureControl(TEMPERATURE_SENSOR_BOX, temp, heatersArray[HEATER_BOX -1]);
    }

    if (statusData->getTemperatureSensorStatus(TEMPERATURE_SENSOR_MOTOR1) == SENS_OK) {
        temp = sensorData->getTemperature(TEMPERATURE_SENSOR_MOTOR1);
        temperatureControl(TEMPERATURE_SENSOR_MOTOR1, temp, heatersArray[HEATER_MOTOR1 - 1]);
    }

    if (statusData->getTemperatureSensorStatus(TEMPERATURE_SENSOR_MOTOR2) == SENS_OK) {
        temp = sensorData->getTemperature(TEMPERATURE_SENSOR_MOTOR2);
        temperatureControl(TEMPERATURE_SENSOR_MOTOR2, temp, heatersArray[HEATER_MOTOR2 - 1]);
    }

    if (statusData->getTemperatureSensorStatus(TEMPERATURE_SENSOR_SERVOS) == SENS_OK) {
        temp = sensorData->getTemperature(TEMPERATURE_SENSOR_SERVOS);
        temperatureControl(TEMPERATURE_SENSOR_SERVOS, temp, heatersArray[HEATER_SERVOS - 1]);
    }

    if (statusData->getTemperatureSensorStatus(TEMPERATURE_SENSOR_CAMERAS) == SENS_OK) {
        temp = sensorData->getTemperature(TEMPERATURE_SENSOR_CAMERAS);
        temperatureControl(TEMPERATURE_SENSOR_CAMERAS, temp, heatersArray[HEATER_CAMERAS - 1]);
    }
}

void AutonomousImpl::temperatureControl(unsigned int numSensor, float temperature, unsigned int heaterAddr)
{
    if ( temperature < _min_temperatures[numSensor - 1] && !_actuator->isHeaterOn(heaterAddr)) // turn the heater on.
    {
        _store->log(INFO_LEVEL, "Heater turned On");
        _actuator->heater(heaterAddr, true);
    }
    else if ( temperature > _max_temperatures[numSensor - 1] && _actuator->isHeaterOn(heaterAddr))
    {
        _store->log(INFO_LEVEL, "Heater turned Off");
        _actuator->heater(heaterAddr, false);
    }
}

void AutonomousImpl::checkTemperatureSensorStatusAndActuate(const StatusData * statusData)
{
    static bool turnedHeaterBoxAutomaticallyOn = false;
    static bool turnedHeaterM1AutomaticallyOn = false;
    static bool turnedHeaterM2AutomaticallyOn = false;
    static bool turnedHeaterServosAutomaticallyOn = false;
    static bool turnedHeaterCamerasAutomaticallyOn = false;

    static time_t heaterBoxTimeAfterChange = now();
    static time_t heaterM1TimeAfterChange = now();
    static time_t heaterM2TimeAfterChange = now();
    static time_t heaterServosTimeAfterChange = now();
    static time_t heaterCamerasTimeAfterChange = now();

    int heatersArray[5] = {HeaterAddr::HEATER_1, HeaterAddr::HEATER_2, HeaterAddr::HEATER_3, HeaterAddr::HEATER_4, HeaterAddr::HEATER_5};

    if (statusData->getTemperatureSensorStatus(TEMPERATURE_SENSOR_BOX) == SENS_ERROR)
    {
        if (statusData->getHeaterStatus(HEATER_BOX) == ACT_ON && (now() - heaterBoxTimeAfterChange) > TEMP_HEATER_CYCLE_ON)
        {
            _actuator->heater(heatersArray[HEATER_BOX - 1], false);
            turnedHeaterBoxAutomaticallyOn = false;
            heaterBoxTimeAfterChange = now();
        }
        else if (statusData->getHeaterStatus(HEATER_BOX) == ACT_OFF && (now() - heaterBoxTimeAfterChange) > TEMP_HEATER_CYCLE_OFF)
        {
            _actuator->heater(heatersArray[HEATER_BOX - 1], true);
            turnedHeaterBoxAutomaticallyOn = true;
            heaterBoxTimeAfterChange = now();
        }
    }
    else if (statusData->getTemperatureSensorStatus(TEMPERATURE_SENSOR_BOX) == SENS_OK && turnedHeaterBoxAutomaticallyOn)
    {
        _actuator->heater(heatersArray[HEATER_BOX - 1], false);
        turnedHeaterBoxAutomaticallyOn = false;
        heaterBoxTimeAfterChange = now();
    }

    if (statusData->getTemperatureSensorStatus(TEMPERATURE_SENSOR_MOTOR1) == SENS_ERROR)
    {
        if (statusData->getHeaterStatus(HEATER_MOTOR1) == ACT_ON && (now() - heaterM1TimeAfterChange) > TEMP_HEATER_CYCLE_ON)
        {
            _actuator->heater(heatersArray[HEATER_MOTOR1 - 1], false);
            turnedHeaterM1AutomaticallyOn = false;
            heaterM1TimeAfterChange = now();
        }
        else if (statusData->getHeaterStatus(HEATER_MOTOR1) == ACT_OFF && (now() - heaterM1TimeAfterChange) > TEMP_HEATER_CYCLE_OFF)
        {
            _actuator->heater(heatersArray[HEATER_MOTOR1 - 1], true);
            turnedHeaterM1AutomaticallyOn = true;
            heaterM1TimeAfterChange = now();
        }
    }
    else if (statusData->getTemperatureSensorStatus(TEMPERATURE_SENSOR_MOTOR1) == SENS_OK && turnedHeaterM1AutomaticallyOn)
    {
        _actuator->heater(heatersArray[HEATER_MOTOR1 - 1], false);
        turnedHeaterM1AutomaticallyOn = false;
        heaterM1TimeAfterChange = now();
    }

    if (statusData->getTemperatureSensorStatus(TEMPERATURE_SENSOR_MOTOR2) == SENS_ERROR)
    {
        if (statusData->getHeaterStatus(HEATER_MOTOR2) == ACT_ON && (now() - heaterM2TimeAfterChange) > TEMP_HEATER_CYCLE_ON)
        {
            _actuator->heater(heatersArray[HEATER_MOTOR2 - 1], false);
            turnedHeaterM2AutomaticallyOn = false;
            heaterM2TimeAfterChange = now();
        }
        else if (statusData->getHeaterStatus(HEATER_MOTOR2) == ACT_OFF && (now() - heaterM2TimeAfterChange) > TEMP_HEATER_CYCLE_OFF)
        {
            _actuator->heater(heatersArray[HEATER_MOTOR2 - 1], true);
            turnedHeaterM2AutomaticallyOn = true;
            heaterM2TimeAfterChange = now();
        }
    }
    else if (statusData->getTemperatureSensorStatus(TEMPERATURE_SENSOR_MOTOR2) == SENS_OK && turnedHeaterM2AutomaticallyOn)
    {
        _actuator->heater(heatersArray[HEATER_MOTOR2 - 1], false);
        turnedHeaterM2AutomaticallyOn = false;
        heaterM2TimeAfterChange = now();
    }

    if (statusData->getTemperatureSensorStatus(TEMPERATURE_SENSOR_SERVOS) == SENS_ERROR)
    {
        if (statusData->getHeaterStatus(HEATER_SERVOS) == ACT_ON && (now() - heaterServosTimeAfterChange) > TEMP_HEATER_CYCLE_ON)
        {
            _actuator->heater(heatersArray[HEATER_SERVOS - 1], false);
            turnedHeaterServosAutomaticallyOn = false;
            heaterServosTimeAfterChange = now();
        }
        else if (statusData->getHeaterStatus(HEATER_SERVOS) == ACT_OFF && (now() - heaterServosTimeAfterChange) > TEMP_HEATER_CYCLE_OFF)
        {
            _actuator->heater(heatersArray[HEATER_SERVOS - 1], true);
            turnedHeaterServosAutomaticallyOn = true;
            heaterServosTimeAfterChange = now();
        }
    }
    else if (statusData->getTemperatureSensorStatus(TEMPERATURE_SENSOR_SERVOS) == SENS_OK && turnedHeaterServosAutomaticallyOn)
    {
        _actuator->heater(heatersArray[HEATER_SERVOS - 1], false);
        turnedHeaterServosAutomaticallyOn = false;
        heaterServosTimeAfterChange = now();
    }

    if (statusData->getTemperatureSensorStatus(TEMPERATURE_SENSOR_CAMERAS) == SENS_ERROR)
    {
        if (statusData->getHeaterStatus(HEATER_CAMERAS) == ACT_ON && (now() - heaterCamerasTimeAfterChange) > TEMP_HEATER_CYCLE_ON)
        {
            _actuator->heater(heatersArray[HEATER_CAMERAS - 1], false);
            turnedHeaterCamerasAutomaticallyOn = false;
            heaterCamerasTimeAfterChange = now();
        }
        else if (statusData->getHeaterStatus(HEATER_CAMERAS) == ACT_OFF && (now() - heaterCamerasTimeAfterChange) > TEMP_HEATER_CYCLE_OFF)
        {
            _actuator->heater(heatersArray[HEATER_CAMERAS - 1], true);
            turnedHeaterCamerasAutomaticallyOn = true;
            heaterCamerasTimeAfterChange = now();
        }
    }
    else if (statusData->getTemperatureSensorStatus(TEMPERATURE_SENSOR_CAMERAS) == SENS_OK && turnedHeaterCamerasAutomaticallyOn)
    {
        _actuator->heater(heatersArray[HEATER_CAMERAS - 1], false);
        turnedHeaterCamerasAutomaticallyOn = false;
        heaterCamerasTimeAfterChange = now();
    }
}

bool AutonomousImpl::releaseAContainer(unsigned int container, SensorData *sensorData, StatusData *statusData, bool reelUpImmediately)
{
    static Releasing_State releasingState = NO_RELEASE;

    static String releasingStateString[6] = {"NO_RELEASE", "PRE_RELEASE", "RELEASE", "REEL_UP", "POST_REEL_UP", "POST_RELEASE"};

    _store->log(INFO_LEVEL, "Releasing State = " + releasingStateString[releasingState] + ". Container " + String(container));

    static const unsigned int timespanAfterCameraOn = 100;
    static time_t timeCameraOn = now();

    static const unsigned int timespanAfterCameraRecording = 10;
    static time_t timeCameraRecording = now();
    static bool waitingCameraToStartRecording = false;

    static const unsigned int timespanAfterServoChanged = 15;
    static time_t timeSinceServoChanged = now();
    static unsigned int numOfTriesLeft = 3;
    static const unsigned int timespanAfterLastServoChanged = 300;
    static time_t timeSinceLastServoChanged = now();

    static const unsigned int timespanBeforeStoppingCamera = 650;
    static const unsigned int timespanAfterReelingUp = 15;
    static time_t timeContainerReeledUp = now();

    static const unsigned int timespanBeforeCameraOff = 380;
    static time_t timeCameraStopped = now();
    static bool waitingCameraToTurnOff = false;

    static bool onGoingRelease = false;

    unsigned int encoderAddress = 0;

    static time_t timeMotorOn = now();

    if (reelUpImmediately)
    {
        _store->log(INFO_LEVEL, "REEL UP INMEDIATELY!!");
        switch (releasingState)
        {
            case RELEASE:
                releasingState = REEL_UP;
                break;
            case PRE_RELEASE:
                _actuator->cameraManagerRecord(false);
                onGoingRelease = false;
                _actuator->cameraManagerPower(false);
                numOfTriesLeft = 3;
                releasingState = NO_RELEASE;
                waitingCameraToStartRecording = false;
                waitingCameraToTurnOff = false;
                break;
        }

    }

    if (releasingState != RELEASE && releasingState != REEL_UP)
    {
        checkTemperatureSensorStatusAndActuate(statusData);
        checkAllTemperatures(sensorData, statusData);
    }
    else
    {
        _actuator->heater(HeaterAddr::HEATER_1, false);
        _actuator->heater(HeaterAddr::HEATER_2, false);
        _actuator->heater(HeaterAddr::HEATER_3, false);
        _actuator->heater(HeaterAddr::HEATER_4, false);
        _actuator->heater(HeaterAddr::HEATER_5, false);
    }


    switch (releasingState)
    {
        case Releasing_State::NO_RELEASE:

            if (statusData->getContainerStatus(container) != CONT_RELEASED && !reelUpImmediately)
            {
                _store->log(INFO_LEVEL, "++ Changing to PRE RELEASE");
                releasingState = PRE_RELEASE;
                onGoingRelease = true;
            }
            break;

        case Releasing_State::PRE_RELEASE:

            if (statusData->getCameraStatus() == CAM_OFF)
            {
                _store->log(INFO_LEVEL, "++ Camera ON");
                _actuator->cameraManagerPower(true);
                timeCameraOn = now();
            }
            else if ((now() - timeCameraOn) > timespanAfterCameraOn && !waitingCameraToStartRecording)
            {
                _store->log(INFO_LEVEL, "++ Camera Recording");
                _actuator->cameraManagerRecord(true);
                timeCameraRecording = now();
                waitingCameraToStartRecording = true;
            }

            if (statusData->getCameraStatus() == CAM_RECORDING && ((now() - timeCameraRecording) > timespanAfterCameraRecording) && waitingCameraToStartRecording)
            {
                _store->log(INFO_LEVEL, "++ Changing to RELEASE");
                releasingState = RELEASE;
                numOfTriesLeft = 3;
                waitingCameraToStartRecording = false;
            }
            else if (statusData->getCameraStatus() != CAM_RECORDING && ((now() - timeCameraRecording) > timespanAfterCameraRecording) && waitingCameraToStartRecording)
            {
                _store->log(INFO_LEVEL, "++ Camera not recording. STOP. Changing to POST RELEASE");
                releasingState = POST_RELEASE;
                waitingCameraToStartRecording = false;
            }

            break;

        case Releasing_State::RELEASE:

            if (numOfTriesLeft > 0)
            {
                _store->log(INFO_LEVEL, "++ Release container " + String(container));
                unsigned int servoAddress = 0;
                switch (container)
                {
                    case 1:
                        servoAddress = ServoAddr::SERVO_1;
                        break;
                    case 2:
                        servoAddress = ServoAddr::SERVO_2;
                        break;
                    case 3:
                        servoAddress = ServoAddr::SERVO_3;
                        break;
                }

                if(statusData->getServoStatus(container) == ACT_OFF && (now() - timeSinceServoChanged) > timespanAfterServoChanged)
                {
                    _actuator->servo(servoAddress, true);

                    if (statusData->getContainerStatus(container) != CONT_RELEASED)
                    {
                        statusData->setContainerStatus(container, CONT_RELEASED);
                        statusData->setContainerReleaseTime(container, now());
                    }
                    timeSinceServoChanged = now();
                }
                else if (statusData->getServoStatus(container) == ACT_ON && (now() - timeSinceServoChanged) > timespanAfterServoChanged)
                {
                    _actuator->servo(servoAddress, false);
                    timeSinceServoChanged = now();
                    numOfTriesLeft--;
                    if (numOfTriesLeft == 0)
                    {
                        timeSinceLastServoChanged = now();
                    }
                }
            }

            if(numOfTriesLeft == 0 && (now() - timeSinceLastServoChanged) > timespanAfterLastServoChanged)
            {
                numOfTriesLeft = 3;
                _store->log(INFO_LEVEL, "++ Changing to REEL UP");
                releasingState = REEL_UP;
            }

            break;

        case Releasing_State::REEL_UP:

            switch (container)
            {
                case 1:
                    encoderAddress = EncoderSensorAddr::ENCODER_1;
                    break;
                case 2:
                    if(statusData->getContainerStatus(3) == CONT_RELEASED)
                    {
                        encoderAddress = EncoderSensorAddr::ENCODER_2;
                    }
                    break;
                case 3:
                    if(statusData->getContainerStatus(2) == CONT_RELEASED)
                    {
                        encoderAddress = EncoderSensorAddr::ENCODER_2;
                    }
                    break;
            }

            if (encoderAddress == 0)
            {
                _store->log(INFO_LEVEL, "++ No reel up. Changing to POST RELEASE");
                releasingState = POST_RELEASE;
                timeContainerReeledUp = now();
            }
            else
            {
                _store->log(INFO_LEVEL, "++ Reeling up container " + String(container));
                unsigned int motorAddress = (container == 1) ? MotorAddr::MOTOR_1 : MotorAddr::MOTOR_2;
                unsigned int numMotor = (container == 3) ? 2 : container;

                switch (statusData->getMotorStatus(numMotor))
                {
                    case ACT_OFF:
                        _store->log(INFO_LEVEL, "++ Turning on the motor! ");
                        _actuator->motor(motorAddress, true);
                        timeMotorOn = now();
                        break;

                    case ACT_ON:
                        checkAndStopReelingUpMotor(encoderAddress);
                        if(numMotor == 1 && (now() - timeMotorOn) >= MAX_TIME_MOTOR_1_RUNNING)
                        {
                            _actuator->motor(motorAddress, false);
                            _store->log(INFO_LEVEL, "++ MAX_TIME_MOTOR_1_RUNNING reached ");
                        }
                        else if (numMotor == 2 && (now() - timeMotorOn) >= MAX_TIME_MOTOR_2_RUNNING)
                        {
                            _actuator->motor(motorAddress, false);
                            _store->log(INFO_LEVEL, "++ MAX_TIME_MOTOR_2_RUNNING reached ");
                        }

                        if (!_actuator->isMotorOn(motorAddress))
                        {
                            _store->log(INFO_LEVEL, "+++ Changing to POST RELEASE");
                            if (numMotor == 1)
                            {
                                releasingState = POST_RELEASE;
                                timeContainerReeledUp = now();
                            }
                            else if (numMotor == 2)
                            {
                                releasingState = POST_REEL_UP;
                            }
                        }
                        break;
                }
            }

            break;

        case Releasing_State::POST_REEL_UP:

            _store->log(INFO_LEVEL, "++ Reeling up container 1");
            switch (statusData->getMotorStatus(1))
            {
                case ACT_OFF:
                    _store->log(INFO_LEVEL, "++ Turning on the motor! ");
                    _actuator->motor(MotorAddr::MOTOR_1, true);
                    timeMotorOn = now();
                    break;

                case ACT_ON:
                    checkAndStopReelingUpMotor(EncoderSensorAddr::ENCODER_1);
                    if((now() - timeMotorOn) >= MAX_TIME_MOTOR_1_RUNNING)
                    {
                        _actuator->motor(MotorAddr::MOTOR_1, false);
                        _store->log(INFO_LEVEL, "++ MAX_TIME_MOTOR_1_RUNNING reached ");
                    }

                    if (!_actuator->isMotorOn(MotorAddr::MOTOR_1))
                    {
                        _store->log(INFO_LEVEL, "+++ Changing to POST RELEASE");
                        releasingState = POST_RELEASE;
                        timeContainerReeledUp = now();
                    }
                    break;
            }

            break;

        case Releasing_State::POST_RELEASE:
            if((now() - timeCameraRecording) > timespanBeforeStoppingCamera && (now() - timeContainerReeledUp) > timespanAfterReelingUp && !waitingCameraToTurnOff)
            {
                _store->log(INFO_LEVEL, "+ Setting camera status to STOP");
                _actuator->cameraManagerRecord(false);
                timeCameraStopped = now();
                waitingCameraToTurnOff = true;
            }
            else if ((now() - timeCameraStopped) > timespanBeforeCameraOff && waitingCameraToTurnOff)
            {
                _store->log(INFO_LEVEL, "+ Changing to NO RELEASE. Releasing finished");
                releasingState = NO_RELEASE;
                onGoingRelease = false;
                _actuator->cameraManagerPower(false);
                waitingCameraToTurnOff = false;
                statusData->setReleaseGapTime(DEFAULT_RELEASE_GAP_TIME);
            }

            break;
    }

    return onGoingRelease;
}

void AutonomousImpl::checkAndStopReelingUpMotor(const unsigned int sensorAddress)
{
    unsigned long encoderPulses = 0;
    switch (sensorAddress)
    {
        case EncoderSensorAddr::ENCODER_1:
            encoderPulses = _sensor->getEncoderState(EncoderSensorAddr::ENCODER_1);
            if (encoderPulses >= NUM_MAX_PULSES_ENCODER_1)
            {
                _actuator->motor(MotorAddr::MOTOR_1, false);
            }
            break;
        case EncoderSensorAddr::ENCODER_2:
            encoderPulses = _sensor->getEncoderState(EncoderSensorAddr::ENCODER_2);
            if (encoderPulses >= NUM_MAX_PULSES_ENCODER_2)
            {
                _actuator->motor(MotorAddr::MOTOR_2, false);
            }
            break;
    }
}
