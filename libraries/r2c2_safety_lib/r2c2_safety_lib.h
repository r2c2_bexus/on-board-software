/*
  r2c2_safety.cpp - Representation of the safety measurements on board of R2C2 BEXUS experiment.
  @author Guillermo Zaragoza Prous
  @date Sept 10, 2019.
  
  Released into the public domain.
*/

#ifndef Safety_lib_h
#define Safety_lib_h

#include <Arduino.h>
#include <r2c2_data.h>
#include <r2c2_store_lib.h>
#include <r2c2_actuator_lib.h>
#include <r2c2_sensor_lib.h>

#define MIN_TEMPERATURE              -5.0  // degrees celsius
#define MAX_TEMPERATURE              -2.0  // degrees celsius

#define TEMP_HEATER_CYCLE_ON         240  // seconds
#define TEMP_HEATER_CYCLE_OFF        600  // seconds

#define TEMPERATURE_SENSOR_BOX       TEMPERATURE_SENS_3_ID
#define HEATER_BOX                   5
#define TEMPERATURE_SENSOR_MOTOR1    TEMPERATURE_SENS_1_ID
#define HEATER_MOTOR1                1
#define TEMPERATURE_SENSOR_MOTOR2    TEMPERATURE_SENS_2_ID
#define HEATER_MOTOR2                2
#define TEMPERATURE_SENSOR_SERVOS    TEMPERATURE_SENS_5_ID
#define HEATER_SERVOS                4
#define TEMPERATURE_SENSOR_CAMERAS   TEMPERATURE_SENS_4_ID
#define HEATER_CAMERAS               3

#define MIN_ALTITUDE                 20000 // metres
#define FLOATING_ALTITUDE            25000 // metres

#define MAX_TIME_MOTOR_1_RUNNING     105 // Half way to 220 seconds
#define MAX_TIME_MOTOR_2_RUNNING      25

/**
 *@class Safety
 *
 * Allows the mind to perform all the safety checks everytime they are needed.
 */
class Safety
{
    public:
        Safety(Store * store);

        /**
         * Checks that it is safe to release a container.
         * @param sensorData Data from the sensors
         * @param statusData Data about the status of the experiment
         * @return true if it is safe, false otherwise
         */
        virtual bool checkPreReleaseSafetyRules(const SensorData *sensorData, const StatusData *statusData) const = 0;

        /**
         * Checks if the heaters of the Motors are functioning to decrease the probability of a failure while reeling up
         * the containers.
         * @param sensorData Data from the sensors
         * @param containerReleased Identifier of the container released
         * @return true if it is safe, false otherwise
         */
        virtual bool checkCriticalSafetyRules(const SensorData *sensorData, int containerReleased) const = 0;

        /**
         * Checks if the experiment is in the ascending phase or it is already in the flotation phase.
         * @param sensorData Data from the sensors
         * @param statusData Data about the status of the experiment
         * @return true if it is ascending, false otherwise
         */
        virtual bool checkIfAscendingPhase(const SensorData* sensorData, const StatusData *statusData) const = 0;

        /**
         * Obtains the current value for each of the sensors and copies them in the SensorData instance passed as an
         * argument to the function. Also checks the status of each of the sensors and updates their status in the
         * StatusData instance passed as an argument.
         * @param sensor Driver that controls the sensors
         * @param sensorData Representation of the data from the sensors.
         * @param statusData Representation of the status of the experiment
         */
        virtual void getValueAndCheckSensorStatuses(Sensor* sensor, SensorData* sensorData, StatusData* statusData) const = 0;

        /**
         * Updates the status of the actuators on the StatusData instance passed depending on the information gathered
         * from the Actuator and Sensor drivers.
         * @param actuator driver for the actuators of the experiment
         * @param sensor driver for the sensors of the experiment
         * @param statusData Representation of the status of the experiment
         */
        virtual void checkActuatorStatuses(Actuator* actuator, Sensor * sensor, StatusData* statusData) const = 0;

        /**
         * Updates the general experiment status depending on the status of all the subsystems on board.
         * @param statusData  Representation of the status of the experiment
         */
        virtual void checkExperimentStatus(StatusData* statusData) const = 0;

    protected:
        Store* _store;
};

class SafetyImpl : public Safety
{
    public:
        SafetyImpl(Store * store);

        bool checkPreReleaseSafetyRules(const SensorData *sensorData, const StatusData *statusData) const;
        bool checkCriticalSafetyRules(const SensorData *sensorData, int containerReleased) const;

        bool checkIfAscendingPhase(const SensorData* sensorData, const StatusData *statusData) const;

        void getValueAndCheckSensorStatuses(Sensor* sensor, SensorData* sensorData, StatusData* statusData) const;
        void checkActuatorStatuses(Actuator* actuator, Sensor * sensor, StatusData* statusData) const;

        void checkExperimentStatus(StatusData* statusData) const;

    private:

        bool rule1CheckAltitudeForRelease(const SensorData* sensorData) const;
        bool rule2CheckReleaseTimeGap(const StatusData* statusData) const;
        bool rule3AMotor1HeaterFunctioning(const SensorData* sensorData) const;
        bool rule3BMotor2HeaterFunctioning(const SensorData* sensorData) const;
};

class Autonomous {
public:
    Autonomous(Store * store, Actuator * actuator, Sensor * sensor);

    virtual void checkAllTemperatures(SensorData * sensorData, StatusData * statusData) = 0;
    virtual void temperatureControl(unsigned int numSensor, float temperature, unsigned int heaterAddr) = 0;

    virtual void checkTemperatureSensorStatusAndActuate(const StatusData * statusData) = 0;

    virtual bool releaseAContainer(unsigned int container, SensorData *sensorData, StatusData* statusData, bool reelUpImmediately = false) = 0;
    virtual void checkAndStopReelingUpMotor(const unsigned int sensorAddress) = 0;

    float getMinTemperature(unsigned int numSensor) const;
    void setMinTemperature(unsigned int numSensor, float temperature);

    float getMaxTemperature(unsigned int numSensor) const;
    void setMaxTemperature(unsigned int numSensor, float temperature);

protected:

    Store* _store;
    Actuator* _actuator;
    Sensor* _sensor;

    float _min_temperatures[NUM_TEMPERATURE_SENSORS];
    float _max_temperatures[NUM_TEMPERATURE_SENSORS];

};

class AutonomousImpl : public Autonomous
{
public:
    AutonomousImpl(Store * store, Actuator * actuator, Sensor * sensor);

    void checkAllTemperatures(SensorData * sensorData, StatusData * statusData);
    void temperatureControl(unsigned int numSensor, float temperature, unsigned int heaterAddr);

    void checkTemperatureSensorStatusAndActuate(const StatusData * statusData);

    bool releaseAContainer(unsigned int container, SensorData *sensorData, StatusData* statusData, bool reelUpImmediately = false);
    void checkAndStopReelingUpMotor(const unsigned int sensorAddress);

};

#endif // Safety_lib_h