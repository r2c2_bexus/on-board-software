
const unsigned int buffer_size = 150;
unsigned int max_size = 0;
const char terminator = '\n';
const char * separator = "$,\n*";
const char * command_str = "GPGLL";

String message;
char buff[buffer_size];


void setup()
{
  Serial.begin(9600);
  Serial.println("Begin GPS Test"); // Text to make sure serial monitor reads at correct baud rate.
  Serial.flush();
  
  Serial1.begin(9600);
}

void loop()
{
  if (Serial1.available() > 0)
  {
    delay(50);
    int size = Serial1.readBytesUntil(terminator, buff, buffer_size);
    // message = Serial1.readStringUntil(terminator);
    // Serial.print((data)); 
    //Serial.println((message));
    /*Serial.print("Message: ");
    Serial.println(buff);*/

    /*if(max_size < size)
    {
      max_size = size;
    }
    
    Serial.print("Size: ");
    Serial.print(size);    
    Serial.print(", Max Size: ");
    Serial.println(max_size); */  

    char * parsed_value;
    parsed_value = strtok(buff,separator);
    /*
    while (parsed_value != NULL)
      {
        Serial.print("Token: ");
        Serial.println(parsed_value);
        parsed_value = strtok(NULL, separator);
      }    
    */
    if(strcmp(parsed_value, command_str) == 0)
    {
      Serial.print("Command: ");
      Serial.println(parsed_value);
      Serial.println("----------");
      parsed_value = strtok(NULL, separator);
      if (parsed_value != NULL)
      {
        Serial.print("Latitude: ");
        Serial.println(parsed_value);
        parsed_value = strtok(NULL, separator);
      }
      if (parsed_value != NULL)
      {
        Serial.print("Hemisphere: ");
        Serial.println(parsed_value);
        parsed_value = strtok(NULL, separator);
      }
      if (parsed_value != NULL)
      {
        Serial.print("Longitude: ");
        Serial.println(parsed_value);
        parsed_value = strtok(NULL, separator);
      }
      if (parsed_value != NULL)
      {
        Serial.print("E/W: ");
        Serial.println(parsed_value);
        parsed_value = strtok(NULL, separator);
      }
    }
    memset (buff, 0, sizeof(buff));
  }
}
