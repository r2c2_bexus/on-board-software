/*
  r2c2_data.cpp - Library in charge of communicate with all the sensors on board of R2C2 BEXUS experiment.
  Created by Guillermo Zaragoza Prous, May 22, 2019.
  Released into the public domain.
*/

#include <stdlib.h>
#include <math.h>

#include <r2c2_sensor_lib.h>

#define WIRE_CLOCK       20000
#define WIRE1_CLOCK     200000

// For the humidity sensor. // TODO: improve implementation.
#define SHT35_MEAS_HIGHREP 0x2400
#define SHT35_MEAS_MEDREP 0x240B
#define SHT35_MEAS_LOWREP 0x2416
#define SHT35_HEATEREN  0x306D    // Heater Enabled
#define SHT35_HEATERDIS 0x3066    // Heater Disabled

// For the IMU and Magnetometer // TODO: improve implementation.

#define AK8963_ST1       0x02
#define AK8963_XOUT_L    0x03
#define AK8963_CNTL      0x0A
#define AK8963_ASAX      0x10

#define XG_OFFSET_H      0x13
#define XG_OFFSET_L      0x14
#define YG_OFFSET_H      0x15
#define YG_OFFSET_L      0x16
#define ZG_OFFSET_H      0x17
#define ZG_OFFSET_L      0x18
#define SMPLRT_DIV       0x19
#define CONFIG           0x1A
#define GYRO_CONFIG      0x1B
#define ACCEL_CONFIG     0x1C
#define ACCEL_CONFIG2    0x1D

#define FIFO_EN          0x23
#define I2C_MST_CTRL     0x24
#define INT_PIN_CFG      0x37
#define INT_ENABLE       0x38
#define INT_STATUS       0x3A
#define ACCEL_XOUT_H     0x3B
#define GYRO_XOUT_H      0x43
#define USER_CTRL        0x6A
#define PWR_MGMT_1       0x6B
#define PWR_MGMT_2       0x6C
#define FIFO_COUNTH      0x72
#define FIFO_R_W         0x74
#define XA_OFFSET_H      0x77
#define XA_OFFSET_L      0x78
#define YA_OFFSET_H      0x7A
#define YA_OFFSET_L      0x7B
#define ZA_OFFSET_H      0x7D
#define ZA_OFFSET_L      0x7E

enum Ascale {
    AFS_2G = 0,
    AFS_4G,
    AFS_8G,
    AFS_16G
};

enum Gscale {
    GFS_250DPS = 0,
    GFS_500DPS,
    GFS_1000DPS,
    GFS_2000DPS
};

enum Mscale {
    MFS_14BITS = 0, // 0.6 mG per LSB
    MFS_16BITS      // 0.15 mG per LSB
};

static uint8_t Gscale = GFS_2000DPS;
static uint8_t Ascale = AFS_2G;
static uint8_t Mscale = MFS_16BITS;
uint8_t Mmode = 0x02;
float _aRes;
float _gRes;
float _mRes;

void getMres() {
    switch (Mscale) {
        // Possible magnetometer scales (and their register bit settings) are:
        // 14 bit resolution (0) and 16 bit resolution (1)
        case MFS_14BITS:
            _mRes = 10. * 4912. / 8190.; // Proper scale to return milliGauss
            break;
        case MFS_16BITS:
            _mRes = 10. * 4912. / 32760.0; // Proper scale to return milliGauss
            break;
    }
}

void getGres() {
    switch (Gscale) {
        // Possible gyro scales (and their register bit settings) are:
        // 250 DPS (00), 500 DPS (01), 1000 DPS (10), and 2000 DPS  (11).
        // Here's a bit of an algorith to calculate DPS/(ADC tick) based on that 2-bit value:
        case GFS_250DPS:
            _gRes = 250.0 / 32768.0;
            break;
        case GFS_500DPS:
            _gRes = 500.0 / 32768.0;
            break;
        case GFS_1000DPS:
            _gRes = 1000.0 / 32768.0;
            break;
        case GFS_2000DPS:
            _gRes = 2000.0 / 32768.0;
            break;
    }
}

void getAres() {
    switch (Ascale) {
        // Possible accelerometer scales (and their register bit settings) are:
        // 2 Gs (00), 4 Gs (01), 8 Gs (10), and 16 Gs  (11).
        // Here's a bit of an algorith to calculate DPS/(ADC tick) based on that 2-bit value:
        case AFS_2G:
            _aRes = 2.0 / 32768.0;
            break;
        case AFS_4G:
            _aRes = 4.0 / 32768.0;
            break;
        case AFS_8G:
            _aRes = 8.0 / 32768.0;
            break;
        case AFS_16G:
            _aRes = 16.0 / 32768.0;
            break;
    }
}

// End IMU and Magnetometer

volatile uint32_t SensorImpl::_encoderValue1 = 0;
volatile uint32_t SensorImpl::_encoderValue2 = 0;

uint8_t crc8(const uint8_t *data, int len);

Sensor::Position::Position(float newLongitude, float newLatitude, unsigned int newAltitude, bool isValid)
{
		longitude = newLongitude;
		latitude = newLatitude;
		altitude = newAltitude;
		validity = isValid;
}

Sensor::Inclination::Inclination(float newAccX, float newAccY, float newAccZ,
                                 float newMagX, float newMagY, float newMagZ,
                                 float newGyrX, float newGyrY, float newGyrZ, bool isValid)
{
    accX = newAccX;
    accY = newAccY;
    accZ = newAccZ;
    magX = newMagX;
    magY = newMagY;
    magZ = newMagZ;
    gyrX = newGyrX;
    gyrY = newGyrY;
    gyrZ = newGyrZ;
    validity = isValid;
}

Sensor::Sensor(Store * store): _store(store)
{

}

SensorImpl::SensorImpl(Store * store): Sensor(store)
{
    setupSerialGPSDispositive();

    Wire.begin();
    Wire.setClock(WIRE_CLOCK);

    setupI2CTempDispositive(TempSensorAddr::TEMP_1);
    setupI2CTempDispositive(TempSensorAddr::TEMP_2);
    setupI2CTempDispositive(TempSensorAddr::TEMP_3);
    setupI2CTempDispositive(TempSensorAddr::TEMP_4);
    setupI2CTempDispositive(TempSensorAddr::TEMP_5);


    setupDetectorSwitchDispositive(DETECTOR_SWITCH_1_PIN);
    setupDetectorSwitchDispositive(DETECTOR_SWITCH_2_PIN);
    setupDetectorSwitchDispositive(DETECTOR_SWITCH_3_PIN);

    setupAnalogThermistorDispositive();

    Wire1.begin();
    Wire1.setClock(WIRE1_CLOCK);

    setupI2CPressDispositive(PressSensorAddr::PRESS_1);

    setupIMUDispositive();
    setupInterruptionEncoderDispositive();
    setupCameraSensorDispositive();
}

float SensorImpl::getTemperature(int address)
{
    if (address == TempSensorAddr::TEMP_5)
    {
        return getThermistor2Temperature();
    }

    // Read Temperature
    Wire.beginTransmission(address);
    Wire.write((int)(0xAA)); // @AA : Temperature
    Wire.endTransmission();
    Wire.requestFrom(address,2); // READ 2 bytes
    Wire.available(); // 1st byte
    float Th = Wire.read(); // receive a byte
    Wire.available(); // 2nd byte
    float Tl = Wire.read(); // receive a byte

    // T° processing
    if(Th>=0x80) //if sign bit is set, then temp is negative
    {
        Th = Th - 256.0;
    }


    float T_dec = (10.0*(100.0*(Tl/16.0)))/16.0; // decimal part of the T°

    Th = Th + (T_dec/1000.0);

    return Th;
}

float SensorImpl::getThermistorTemperature()
{
    static const float A = 3.908;
    static const float B = -0.0005775;
    static const float m = 233.3333;
    static const float C = 630.3333;

    float Vo = (analogRead(THERMISTOR_PIN)/1023.0)*3.257;

    float R2 = m * Vo + C;

    float T = (-0.5*A/B) - sqrt((0.5*A/B)*(0.5*A/B) - (1000.0 - R2)/B);
    return T;
}

float SensorImpl::getThermistor2Temperature()
{
    static const float A = 0.0039803;
    static const float B = -0.0000005775;
    static const float m = 14.618;
    static const float C = 78.69091;
    static const __uint8_t R00 = 100;

    float Vo = (analogRead(TempSensorAddr::TEMP_5)/1023.0)*3.257;

    float Rt = m * Vo + C;

    float T1 = (-R00 * A) + sqrt(pow(R00*A,2) - 4 * R00 * B * (R00 - Rt));    // <- Check!
    float T2 = 2 * R00 * B;
    // float T = (-0.5*A/B) - sqrt((0.5*A/B)*(0.5*A/B) - (1000.0 - R2)/B);
    float T = T1/T2;
    return T;
}

Sensor::Position SensorImpl::getPosition()
{

	static const unsigned int buffer_size = 150;
    static const char terminator = '\n';
    static const char * separator = "$,\n*";

    static const char * command_str = "GPGGA";
    static const unsigned int numCommandFields = 16;
    static const unsigned int utcTimeField = 1;
    static const unsigned int latitudeField = 2;
    static const unsigned int northSouthField = 3;
    static const unsigned int longitudeField = 4;
    static const unsigned int eastWestField = 5;
    static const unsigned int validityField = 6;
    static const unsigned int altitudeField = 9;

	char buff[buffer_size];

	bool newPosition = false;
	int  numOfTries  = 10;

	Sensor::Position position(0.0, 0.0, 0);

	while (_gpsSerial->available() > 0 && !newPosition && numOfTries > 0)
	  {
	    delay(50);
	    int size = _gpsSerial->readBytesUntil(terminator, buff, buffer_size);

	    /*Serial.println("----------");
        Serial.print("Received command: ");
        Serial.println(buff);*/

	    char * parsed_value;
	    parsed_value = strtok(buff, separator);

	    if(strcmp(parsed_value, command_str) == 0)
	    {
	      /*Serial.println("----------");
	      Serial.print("Command code: ");
	      Serial.println(parsed_value);*/

	      unsigned int fieldCounter = 1;
	      bool furtherValidityChecking = true;
	      while(fieldCounter <= numCommandFields)
          {
              parsed_value = strtok(NULL, separator);
              if (parsed_value != NULL)
              {
                  long int longAltitude = -1;
                  switch (fieldCounter)
                  {
                      /*case utcTimeField:
                          Serial.print("UTC Time: ");
                          Serial.println(parsed_value);
                          break;*/

                      case latitudeField:
                          /*Serial.print("Latitude: ");
                          Serial.println(parsed_value);*/
                          position.latitude = atof(parsed_value);
                          break;

                      case northSouthField:
                          /*Serial.print("Hemisphere: ");
                          Serial.println(parsed_value);*/
                          if (strcmp(parsed_value, "S") == 0)
                          {
                              position.latitude = -position.latitude;
                          }
                          furtherValidityChecking = (strcmp(parsed_value, "N") == 0 || strcmp(parsed_value, "S") == 0);
                          break;

                      case longitudeField:
                          /*Serial.print("Longitude: ");
                          Serial.println(parsed_value);*/
                          position.longitude = atof(parsed_value);
                          break;

                      case eastWestField:
                          /*Serial.print("E/W: ");
                          Serial.println(parsed_value);*/
                          if (strcmp(parsed_value, "W") == 0)
                          {
                              position.longitude = -position.longitude;
                          }
                          furtherValidityChecking = (strcmp(parsed_value, "E") == 0 || strcmp(parsed_value, "W") == 0);
                          break;

                      case altitudeField:
                          /*Serial.print("Altitude: ");
                          Serial.println(parsed_value);*/
                          longAltitude = atol(parsed_value);
                          position.altitude = (unsigned int) longAltitude;
                          break;

                      case validityField:
                          /*Serial.print("Validity (Position Fix Status): ");
                          Serial.println(parsed_value);*/
                          position.validity = (strcmp(parsed_value, "1") == 0 || strcmp(parsed_value, "2") == 0 || strcmp(parsed_value, "6") == 0);
                          if(!furtherValidityChecking)
                          {
                              position.validity = furtherValidityChecking;
                          }
                          newPosition = position.validity;
                          break;
                  }
              }
	          fieldCounter++;
          }
	    }
	    memset (buff, 0, sizeof(buff));
	    numOfTries--;
	  }

	  return position;
}

float SensorImpl::getHumidity()
{
    float shum = 0;

    sendI2CCommand16(HumidSensorAddr::HUMID_1, SHT35_MEAS_HIGHREP);
    delay(20);

    Wire1.requestFrom(HumidSensorAddr::HUMID_1, 6);

    if (Wire1.available() != 6)
        return shum;

    uint8_t readBuffer[6];
    for (uint8_t i = 0; i < 6; i++){
        readBuffer[i] = Wire1.read();
    }

    uint16_t SRH;

    SRH = readBuffer[3];
    SRH <<= 8;
    SRH |= readBuffer[4];

    if (readBuffer[5] != crc8(readBuffer + 3, 2))
        return 0;

    shum = SRH;
    shum *= 100.0;
    shum /= 0xFFFF;

    return shum;
}

unsigned int SensorImpl::getPressure()
{

    uint32_t rawPressure = readPressureSensor(PressSensorAddr::PRESS_1, 0x48);  // Raw Pressure Data
    uint32_t rawTemperature = readPressureSensor(PressSensorAddr::PRESS_1, 0x58);  // Raw Temperature Data

    double dT = 0.0;
    double TEMP = 0.0;
    double OFF  = 0.0;
    double SENS = 0.0;
    double P = 0.0;

    double Tt = 0.0;
    double Pp = 0.0;

    static const double pow2_8 = pow(2,8);
    static const double pow2_23 = pow(2,23);
    static const double pow2_17 = pow(2,17);
    static const double pow2_6 = pow(2,6);
    static const double pow2_16 = pow(2,16);
    static const double pow2_7 = pow(2,7);
    static const double pow2_21 = pow(2,21);
    static const double pow2_15 = pow(2,15);

    if (rawPressure != 0)
    {

        dT = (double)rawTemperature - ((double)_pressureCoeficients[4]*(pow2_8));
        TEMP = (2000.0 + ((dT * (double)_pressureCoeficients[5])/pow2_23));
        OFF  = (_pressureCoeficients[1]*pow2_17 + ((_pressureCoeficients[3] * dT)/pow2_6));
        SENS = (((double)_pressureCoeficients[0])*pow2_16) + (((double)_pressureCoeficients[2] * dT)/pow2_7);
        P = (((double)rawPressure * SENS/pow2_21 - OFF)/pow2_15);

        Tt = TEMP/100.0; // C
        Pp = P/100.0; // mbar
    }

    return Pp;
}

unsigned int SensorImpl::getContainerState(const unsigned int numSensor)
{
    unsigned int containerState = 0;
    int senseValue = 0;

    switch (numSensor)
    {
        case 1:
            senseValue = analogRead(DETECTOR_SWITCH_1_PIN);
            break;
        case 2:
            senseValue = analogRead(DETECTOR_SWITCH_2_PIN);
            break;
        case 3:
            senseValue = analogRead(DETECTOR_SWITCH_3_PIN);
            break;
    }

    if(senseValue > 512){ // Stowed
        containerState = 0;
    }

    else { // Released
        containerState = 1;
    }

    return containerState;
}

uint32_t SensorImpl::getEncoderState(const unsigned int sensorAddress)
{
    uint32_t encoderState = 0;
    switch (sensorAddress)
    {
        case EncoderSensorAddr::ENCODER_1:
            encoderState = _encoderValue1;
            break;
        case EncoderSensorAddr::ENCODER_2:
            encoderState = _encoderValue2;
            break;
    }
    return encoderState;
}

bool SensorImpl::isCameraRecording()
{
    bool cameraRecording = false;
    int senseValue = digitalRead(CameraSensorAddr::CAMERA_1);

    if(senseValue == HIGH){ // Stopped
        cameraRecording = false;
    }
    else { // Recording
        cameraRecording = true;
    }

    return cameraRecording;
}

void SensorImpl::IMUwriteByte(uint8_t address, uint8_t subAddress, uint8_t data) {
    Wire1.beginTransmission(address);  // Initialize the Tx buffer
    Wire1.write(subAddress);           // Put slave register address in Tx buffer
    Wire1.write(data);                 // Put data in Tx buffer
    Wire1.endTransmission();           // Send the Tx buffer
}

uint8_t SensorImpl::IMUreadByte(uint8_t address, uint8_t subAddress) {
    uint8_t data; // `data` will store the register data
    Wire1.beginTransmission(address);         // Initialize the Tx buffer
    Wire1.write(subAddress);                  // Put slave register address in Tx buffer
    Wire1.endTransmission(false);             // Send the Tx buffer, but send a restart to keep connection alive
    Wire1.requestFrom(address, (uint8_t) 1);  // Read one byte from slave register address
    data = Wire1.read();                      // Fill Rx buffer with result
    return data;                             // Return data read from slave register
}

void SensorImpl::IMUreadBytes(uint8_t address, uint8_t subAddress, uint8_t count, uint8_t *dest) {
    Wire1.beginTransmission(address);   // Initialize the Tx buffer
    Wire1.write(subAddress);            // Put slave register address in Tx buffer
    Wire1.endTransmission(false);       // Send the Tx buffer, but send a restart to keep connection alive
    uint8_t i = 0;
    Wire1.requestFrom(address, count);  // Read bytes from slave register address
    while (Wire1.available()) {
        dest[i++] = Wire1.read();
    }         // Put read results in the Rx buffer
}

void SensorImpl::readAccelData(int16_t *destination) {
    uint8_t rawData[6];  // x/y/z accel register data stored here
    IMUreadBytes(IMUSensorAddr::IMU_1, ACCEL_XOUT_H, 6, &rawData[0]);  // Read the six raw data registers into data array
    destination[0] = ((int16_t) rawData[0] << 8) | rawData[1];  // Turn the MSB and LSB into a signed 16-bit value
    destination[1] = ((int16_t) rawData[2] << 8) | rawData[3];
    destination[2] = ((int16_t) rawData[4] << 8) | rawData[5];
}

void SensorImpl::readGyroData(int16_t *destination) {
    uint8_t rawData[6];  // x/y/z gyro register data stored here
    IMUreadBytes(IMUSensorAddr::IMU_1, GYRO_XOUT_H, 6, &rawData[0]);  // Read the six raw data registers sequentially into data array
    destination[0] = ((int16_t) rawData[0] << 8) | rawData[1];  // Turn the MSB and LSB into a signed 16-bit value
    destination[1] = ((int16_t) rawData[2] << 8) | rawData[3];
    destination[2] = ((int16_t) rawData[4] << 8) | rawData[5];
}

void SensorImpl::readMagData(int16_t *destination) {
    uint8_t rawData[7];  // x/y/z gyro register data, ST2 register stored here, must read ST2 at end of data acquisition
    if (IMUreadByte(MagnetometerSensorAddr::MAG_1, AK8963_ST1) & 0x01) { // wait for magnetometer data ready bit to be set
        IMUreadBytes(MagnetometerSensorAddr::MAG_1, AK8963_XOUT_L, 7, &rawData[0]);  // Read the six raw data and ST2 registers sequentially into data array
        uint8_t c = rawData[6]; // End data read by reading ST2 register
        if (!(c & 0x08)) { // Check if magnetic sensor overflow set, if not then report data
            destination[0] =
                    ((int16_t) rawData[1] << 8) | rawData[0];  // Turn the MSB and LSB into a signed 16-bit value
            destination[1] = ((int16_t) rawData[3] << 8) | rawData[2];  // Data stored as little Endian
            destination[2] = ((int16_t) rawData[5] << 8) | rawData[4];
        }
    }
}

Sensor::Inclination SensorImpl::getInclination()
{
    Sensor::Inclination inclination(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);

    //if (readByte(IMUSensorAddr::IMU_1, INT_STATUS) & 0x01) {  // On interrupt, check if data ready interrupt

        int16_t accelCount[3];  // Stores the 16-bit signed accelerometer sensor output
        int16_t gyroCount[3];   // Stores the 16-bit signed gyro sensor output
        int16_t magCount[3];
        readAccelData(accelCount);  // Read the x/y/z adc values
        getAres();

        // Now we'll calculate the accleration value into actual g's
        inclination.accX = (float) accelCount[0] * _aRes; // - accelBias[0];  // get actual g value, this depends on scale being set
        inclination.accY = (float) accelCount[1] * _aRes; // - accelBias[1];
        inclination.accZ = (float) accelCount[2] * _aRes; // - accelBias[2];

        readGyroData(gyroCount);  // Read the x/y/z adc values
        getGres();

        // Calculate the gyro value into actual degrees per second
        inclination.gyrX = (float) gyroCount[0] * _gRes;  // get actual gyro value, this depends on scale being set
        inclination.gyrY = (float) gyroCount[1] * _gRes;
        inclination.gyrZ = (float) gyroCount[2] * _gRes;

        readMagData(magCount);  // Read the x/y/z adc values
        getMres();

        // Calculate the magnetometer values in milliGauss
        // Include factory calibration per data sheet and user environmental corrections
        inclination.magX = (float) magCount[0] * _mRes * _magCalibration[0] - _magbias[0];  // get actual magnetometer value, this depends on scale being set
        inclination.magY = (float) magCount[1] * _mRes * _magCalibration[1] - _magbias[1];
        inclination.magZ = (float) magCount[2] * _mRes * _magCalibration[2] - _magbias[2];

        inclination.validity = true;
    //}

    return inclination;
}

void SensorImpl::setupSerialGPSDispositive()
{
    _gpsSerial = &Serial1;
    _gpsSerial->begin(9600);
    _gpsSerial->setTimeout(250);
}

void SensorImpl::setupI2CTempDispositive(int address)
{
    // Stop conversion to be able to modify "Access Config" Register
    Wire.beginTransmission(address);
    Wire.write((int)(0x22));  // Stop conversion
    Wire.endTransmission();

    // Read "Access Config" register
    Wire.beginTransmission(address);
    Wire.write((int)(0xAC));  // @AC : Access Config
    Wire.endTransmission();
    Wire.requestFrom(address,1);  // Read 1 byte
    Wire.available();
    int AC = Wire.read(); // receive a byte

    /*Serial.print("Access Config (Before): ");
    Serial.print(AC);
    Serial.println('\n');*/

    // WRITE into "Access Config" Register
    Wire.beginTransmission(address);
    Wire.write(0xAC); // @AC : Access Config
    Wire.write(0x0C); // Continous conversion & 12 bits resolution
    Wire.endTransmission();

    // READ "Access Config" register
    Wire.beginTransmission(address);
    Wire.write((int)(0xAC));  //@AC : Access Config
    Wire.endTransmission();
    Wire.requestFrom(address,1);
    Wire.available();
    AC = Wire.read();

    /*Serial.print("Access Config (AFTER): ");
    Serial.print(AC);
    Serial.println("");*/

    // START conversion to get T°
    Wire.beginTransmission(address);
    Wire.write((int)(0x51)); // Start Conversion
    Wire.endTransmission();
}

void SensorImpl::setupI2CPressDispositive(int address)
{
    Wire1.beginTransmission(address);
    Wire1.write(0x1E);   // Reset command
    Wire1.endTransmission();
    delay(10);

    for (int i=0; i<6; i++){

        Wire1.beginTransmission(address);
        Wire1.write(0xA2 + (i * 2));
        Wire1.endTransmission();

        Wire1.beginTransmission(address);
        Wire1.requestFrom(address, 6);
        delay(1);

        if(Wire1.available()) {
            _pressureCoeficients[i] = Wire1.read() * 256 + Wire1.read();
        }/*
        else {
            Serial.println("setupI2CPressDispositive: Error reading PROM 1"); // Error reading the PROM or communication with the device
        }*/
    }

}

void SensorImpl::setupAnalogThermistorDispositive()
{
    analogReadResolution(10); // Enable control of the accuracy of the ADC
}

void SensorImpl::setupDetectorSwitchDispositive(int address)
{
    pinMode(address, INPUT);
}

void SensorImpl::setupInterruptionEncoderDispositive()
{
    pinMode(EncoderSensorAddr::ENCODER_1, INPUT_PULLUP);
    attachInterrupt(digitalPinToInterrupt(EncoderSensorAddr::ENCODER_1), &updateEncoder1Callback, RISING);

    pinMode(EncoderSensorAddr::ENCODER_2, INPUT_PULLUP);
    attachInterrupt(digitalPinToInterrupt(EncoderSensorAddr::ENCODER_2), &updateEncoder2Callback, RISING);
}

void SensorImpl::setupCameraSensorDispositive()
{
    pinMode(CameraSensorAddr::CAMERA_1, INPUT);
}

void SensorImpl::calibrateIMU(float *dest1, float *dest2)
{
    uint8_t data[12]; // data array to hold accelerometer and gyro x, y, z, data
    uint16_t ii, packet_count, fifo_count;
    int32_t gyro_bias[3] = {0, 0, 0}, accel_bias[3] = {0, 0, 0};

    // reset device
    IMUwriteByte(IMUSensorAddr::IMU_1, PWR_MGMT_1, 0x80); // Write a one to bit 7 reset bit; toggle reset device
    delay(100);

    // get stable time source; Auto select clock source to be PLL gyroscope reference if ready
    // else use the internal oscillator, bits 2:0 = 001
    IMUwriteByte(IMUSensorAddr::IMU_1, PWR_MGMT_1, 0x01);
    IMUwriteByte(IMUSensorAddr::IMU_1, PWR_MGMT_2, 0x00);
    delay(200);

// Configure device for bias calculation
    IMUwriteByte(IMUSensorAddr::IMU_1, INT_ENABLE, 0x00);   // Disable all interrupts
    IMUwriteByte(IMUSensorAddr::IMU_1, FIFO_EN, 0x00);      // Disable FIFO
    IMUwriteByte(IMUSensorAddr::IMU_1, PWR_MGMT_1, 0x00);   // Turn on internal clock source
    IMUwriteByte(IMUSensorAddr::IMU_1, I2C_MST_CTRL, 0x00); // Disable I2C master
    IMUwriteByte(IMUSensorAddr::IMU_1, USER_CTRL, 0x00);    // Disable FIFO and I2C master modes
    IMUwriteByte(IMUSensorAddr::IMU_1, USER_CTRL, 0x0C);    // Reset FIFO and DMP
    delay(15);

// Configure MPU6050 gyro and accelerometer for bias calculation
    IMUwriteByte(IMUSensorAddr::IMU_1, CONFIG, 0x01);      // Set low-pass filter to 188 Hz
    IMUwriteByte(IMUSensorAddr::IMU_1, SMPLRT_DIV, 0x00);  // Set sample rate to 1 kHz
    IMUwriteByte(IMUSensorAddr::IMU_1, GYRO_CONFIG, 0x00);  // Set gyro full-scale to 250 degrees per second, maximum sensitivity
    IMUwriteByte(IMUSensorAddr::IMU_1, ACCEL_CONFIG, 0x00); // Set accelerometer full-scale to 2 g, maximum sensitivity

    uint16_t gyrosensitivity = 131;   // = 131 LSB/degrees/sec
    uint16_t accelsensitivity = 16384;  // = 16384 LSB/g

    // Configure FIFO to capture accelerometer and gyro data for bias calculation
    IMUwriteByte(IMUSensorAddr::IMU_1, USER_CTRL, 0x40);   // Enable FIFO
    IMUwriteByte(IMUSensorAddr::IMU_1, FIFO_EN, 0x78);     // Enable gyro and accelerometer sensors for FIFO  (max size 512 bytes in MPU-9150)
    delay(40); // accumulate 40 samples in 40 milliseconds = 480 bytes

// At end of sample accumulation, turn off FIFO sensor read
    IMUwriteByte(IMUSensorAddr::IMU_1, FIFO_EN, 0x00);        // Disable gyro and accelerometer sensors for FIFO
    IMUreadBytes(IMUSensorAddr::IMU_1, FIFO_COUNTH, 2, &data[0]); // read FIFO sample count
    fifo_count = ((uint16_t) data[0] << 8) | data[1];
    packet_count = fifo_count / 12;// How many sets of full gyro and accelerometer data for averaging

    for (ii = 0; ii < packet_count; ii++) {
        int16_t accel_temp[3] = {0, 0, 0}, gyro_temp[3] = {0, 0, 0};
        IMUreadBytes(IMUSensorAddr::IMU_1, FIFO_R_W, 12, &data[0]); // read data for averaging
        accel_temp[0] = (int16_t)(
                ((int16_t) data[0] << 8) | data[1]);  // Form signed 16-bit integer for each sample in FIFO
        accel_temp[1] = (int16_t)(((int16_t) data[2] << 8) | data[3]);
        accel_temp[2] = (int16_t)(((int16_t) data[4] << 8) | data[5]);
        gyro_temp[0] = (int16_t)(((int16_t) data[6] << 8) | data[7]);
        gyro_temp[1] = (int16_t)(((int16_t) data[8] << 8) | data[9]);
        gyro_temp[2] = (int16_t)(((int16_t) data[10] << 8) | data[11]);

        accel_bias[0] += (int32_t) accel_temp[0]; // Sum individual signed 16-bit biases to get accumulated signed 32-bit biases
        accel_bias[1] += (int32_t) accel_temp[1];
        accel_bias[2] += (int32_t) accel_temp[2];
        gyro_bias[0] += (int32_t) gyro_temp[0];
        gyro_bias[1] += (int32_t) gyro_temp[1];
        gyro_bias[2] += (int32_t) gyro_temp[2];
    }

    accel_bias[0] /= (int32_t) packet_count; // Normalize sums to get average count biases
    accel_bias[1] /= (int32_t) packet_count;
    accel_bias[2] /= (int32_t) packet_count;
    gyro_bias[0] /= (int32_t) packet_count;
    gyro_bias[1] /= (int32_t) packet_count;
    gyro_bias[2] /= (int32_t) packet_count;

    if (accel_bias[2] >
        0L) { accel_bias[2] -= (int32_t) accelsensitivity; }  // Remove gravity from the z-axis accelerometer bias calculation
    else { accel_bias[2] += (int32_t) accelsensitivity; }

// Construct the gyro biases for push to the hardware gyro bias registers, which are reset to zero upon device startup
    data[0] = (-gyro_bias[0] / 4 >> 8) &
              0xFF; // Divide by 4 to get 32.9 LSB per deg/s to conform to expected bias input format
    data[1] = (-gyro_bias[0] / 4) & 0xFF; // Biases are additive, so change sign on calculated average gyro biases
    data[2] = (-gyro_bias[1] / 4 >> 8) & 0xFF;
    data[3] = (-gyro_bias[1] / 4) & 0xFF;
    data[4] = (-gyro_bias[2] / 4 >> 8) & 0xFF;
    data[5] = (-gyro_bias[2] / 4) & 0xFF;

// Push gyro biases to hardware registers
    IMUwriteByte(IMUSensorAddr::IMU_1, XG_OFFSET_H, data[0]);
    IMUwriteByte(IMUSensorAddr::IMU_1, XG_OFFSET_L, data[1]);
    IMUwriteByte(IMUSensorAddr::IMU_1, YG_OFFSET_H, data[2]);
    IMUwriteByte(IMUSensorAddr::IMU_1, YG_OFFSET_L, data[3]);
    IMUwriteByte(IMUSensorAddr::IMU_1, ZG_OFFSET_H, data[4]);
    IMUwriteByte(IMUSensorAddr::IMU_1, ZG_OFFSET_L, data[5]);

// Output scaled gyro biases for display in the main program
    dest1[0] = (float) gyro_bias[0] / (float) gyrosensitivity;
    dest1[1] = (float) gyro_bias[1] / (float) gyrosensitivity;
    dest1[2] = (float) gyro_bias[2] / (float) gyrosensitivity;

// Construct the accelerometer biases for push to the hardware accelerometer bias registers. These registers contain
// factory trim values which must be added to the calculated accelerometer biases; on boot up these registers will hold
// non-zero values. In addition, bit 0 of the lower byte must be preserved since it is used for temperature
// compensation calculations. Accelerometer bias registers expect bias input as 2048 LSB per g, so that
// the accelerometer biases calculated above must be divided by 8.

    int32_t accel_bias_reg[3] = {0, 0, 0}; // A place to hold the factory accelerometer trim biases
    IMUreadBytes(IMUSensorAddr::IMU_1, XA_OFFSET_H, 2, &data[0]); // Read factory accelerometer trim values
    accel_bias_reg[0] = (int32_t)(((int16_t) data[0] << 8) | data[1]);
    IMUreadBytes(IMUSensorAddr::IMU_1, YA_OFFSET_H, 2, &data[0]);
    accel_bias_reg[1] = (int32_t)(((int16_t) data[0] << 8) | data[1]);
    IMUreadBytes(IMUSensorAddr::IMU_1, ZA_OFFSET_H, 2, &data[0]);
    accel_bias_reg[2] = (int32_t)(((int16_t) data[0] << 8) | data[1]);

    uint32_t mask = 1uL; // Define mask for temperature compensation bit 0 of lower byte of accelerometer bias registers
    uint8_t mask_bit[3] = {0, 0, 0}; // Define array to hold mask bit for each accelerometer bias axis

    for (ii = 0; ii < 3; ii++) {
        if ((accel_bias_reg[ii] & mask)) {
            mask_bit[ii] = 0x01;
        } // If temperature compensation bit is set, record that fact in mask_bit
    }

    // Construct total accelerometer bias, including calculated average accelerometer bias from above
    accel_bias_reg[0] -= (accel_bias[0] /
                          8); // Subtract calculated averaged accelerometer bias scaled to 2048 LSB/g (16 g full scale)
    accel_bias_reg[1] -= (accel_bias[1] / 8);
    accel_bias_reg[2] -= (accel_bias[2] / 8);

    data[0] = (accel_bias_reg[0] >> 8) & 0xFF;
    data[1] = (accel_bias_reg[0]) & 0xFF;
    data[1] = data[1] |
              mask_bit[0]; // preserve temperature compensation bit when writing back to accelerometer bias registers
    data[2] = (accel_bias_reg[1] >> 8) & 0xFF;
    data[3] = (accel_bias_reg[1]) & 0xFF;
    data[3] = data[3] |
              mask_bit[1]; // preserve temperature compensation bit when writing back to accelerometer bias registers
    data[4] = (accel_bias_reg[2] >> 8) & 0xFF;
    data[5] = (accel_bias_reg[2]) & 0xFF;
    data[5] = data[5] |
              mask_bit[2]; // preserve temperature compensation bit when writing back to accelerometer bias registers

// Apparently this is not working for the acceleration biases in the MPU-9250
// Are we handling the temperature correction bit properly?
// Push accelerometer biases to hardware registers
    IMUwriteByte(IMUSensorAddr::IMU_1, XA_OFFSET_H, data[0]);
    IMUwriteByte(IMUSensorAddr::IMU_1, XA_OFFSET_L, data[1]);
    IMUwriteByte(IMUSensorAddr::IMU_1, YA_OFFSET_H, data[2]);
    IMUwriteByte(IMUSensorAddr::IMU_1, YA_OFFSET_L, data[3]);
    IMUwriteByte(IMUSensorAddr::IMU_1, ZA_OFFSET_H, data[4]);
    IMUwriteByte(IMUSensorAddr::IMU_1, ZA_OFFSET_L, data[5]);

// Output scaled accelerometer biases for display in the main program
    dest2[0] = (float) accel_bias[0] / (float) accelsensitivity;
    dest2[1] = (float) accel_bias[1] / (float) accelsensitivity;
    dest2[2] = (float) accel_bias[2] / (float) accelsensitivity;
}

void SensorImpl::initIMU() {

    // wake up device
    IMUwriteByte(IMUSensorAddr::IMU_1, PWR_MGMT_1, 0x00); // Clear sleep mode bit (6), enable all sensors
    delay(100); // Wait for all registers to reset

    // get stable time source
    IMUwriteByte(IMUSensorAddr::IMU_1, PWR_MGMT_1,
              0x01);  // Auto select clock source to be PLL gyroscope reference if ready else
    delay(200);

    // Configure Gyro and Thermometer
    // Disable FSYNC and set thermometer and gyro bandwidth to 41 and 42 Hz, respectively;
    // minimum delay time for this setting is 5.9 ms, which means sensor fusion update rates cannot
    // be higher than 1 / 0.0059 = 170 Hz
    // DLPF_CFG = bits 2:0 = 011; this limits the sample rate to 1000 Hz for both
    // With the MPU9250, it is possible to get gyro sample rates of 32 kHz (!), 8 kHz, or 1 kHz
    IMUwriteByte(IMUSensorAddr::IMU_1, CONFIG, 0x03);

    // Set sample rate = gyroscope output rate/(1 + SMPLRT_DIV)
    IMUwriteByte(IMUSensorAddr::IMU_1, SMPLRT_DIV, 0x04);  // Use a 200 Hz rate; a rate consistent with the filter update rate
    // determined inset in CONFIG above

    // Set gyroscope full scale range
    // Range selects FS_SEL and GFS_SEL are 0 - 3, so 2-bit values are left-shifted into positions 4:3
    uint8_t c = IMUreadByte(IMUSensorAddr::IMU_1, GYRO_CONFIG); // get current GYRO_CONFIG register value
    // c = c & ~0xE0; // Clear self-test bits [7:5]
    c = c & ~0x03; // Clear Fchoice bits [1:0]
    c = c & ~0x18; // Clear GFS bits [4:3]
    c = c | Gscale << 3; // Set full scale range for the gyro
    // c =| 0x00; // Set Fchoice for the gyro to 11 by writing its inverse to bits 1:0 of GYRO_CONFIG
    IMUwriteByte(IMUSensorAddr::IMU_1, GYRO_CONFIG, c); // Write new GYRO_CONFIG value to register

    // Set accelerometer full-scale range configuration
    c = IMUreadByte(IMUSensorAddr::IMU_1, ACCEL_CONFIG); // get current ACCEL_CONFIG register value
    // c = c & ~0xE0; // Clear self-test bits [7:5]
    c = c & ~0x18;  // Clear AFS bits [4:3]
    c = c | Ascale << 3; // Set full scale range for the accelerometer
    IMUwriteByte(IMUSensorAddr::IMU_1, ACCEL_CONFIG, c); // Write new ACCEL_CONFIG register value

    // Set accelerometer sample rate configuration
    // It is possible to get a 4 kHz sample rate from the accelerometer by choosing 1 for
    // accel_fchoice_b bit [3]; in this case the bandwidth is 1.13 kHz
    c = IMUreadByte(IMUSensorAddr::IMU_1, ACCEL_CONFIG2); // get current ACCEL_CONFIG2 register value
    c = c & ~0x0F; // Clear accel_fchoice_b (bit 3) and A_DLPFG (bits [2:0])
    c = c | 0x03;  // Set accelerometer rate to 1 kHz and bandwidth to 41 Hz
    IMUwriteByte(IMUSensorAddr::IMU_1, ACCEL_CONFIG2, c); // Write new ACCEL_CONFIG2 register value
    // The accelerometer, gyro, and thermometer are set to 1 kHz sample rates,
    // but all these rates are further reduced by a factor of 5 to 200 Hz because of the SMPLRT_DIV setting

    // Configure Interrupts and Bypass Enable
    // Set interrupt pin active high, push-pull, hold interrupt pin level HIGH until interrupt cleared,
    // clear on read of INT_STATUS, and enable I2C_BYPASS_EN so additional chips
    // can join the I2C bus and all can be controlled by the Arduino as master
    IMUwriteByte(IMUSensorAddr::IMU_1, INT_PIN_CFG, 0x22);
    IMUwriteByte(IMUSensorAddr::IMU_1, INT_ENABLE, 0x01);  // Enable data ready (bit 0) interrupt
    delay(100);
}

void SensorImpl::initMagnetometer(float *destination) {
    // First extract the factory calibration for each magnetometer axis
    uint8_t rawData[3];  // x/y/z gyro calibration data stored here
    IMUwriteByte(MagnetometerSensorAddr::MAG_1, AK8963_CNTL, 0x00); // Power down magnetometer
    delay(10);
    IMUwriteByte(MagnetometerSensorAddr::MAG_1, AK8963_CNTL, 0x0F); // Enter Fuse ROM access mode
    delay(10);
    IMUreadBytes(MagnetometerSensorAddr::MAG_1, AK8963_ASAX, 3, &rawData[0]);  // Read the x-, y-, and z-axis calibration values
    destination[0] = (float) (rawData[0] - 128) / 256. + 1.;   // Return x-axis sensitivity adjustment values, etc.
    destination[1] = (float) (rawData[1] - 128) / 256. + 1.;
    destination[2] = (float) (rawData[2] - 128) / 256. + 1.;
    IMUwriteByte(MagnetometerSensorAddr::MAG_1, AK8963_CNTL, 0x00); // Power down magnetometer
    delay(10);
    // Configure the magnetometer for continuous read and highest resolution
    // set Mscale bit 4 to 1 (0) to enable 16 (14) bit resolution in CNTL register,
    // and enable continuous mode data acquisition Mmode (bits [3:0]), 0010 for 8 Hz and 0110 for 100 Hz sample rates
    IMUwriteByte(MagnetometerSensorAddr::MAG_1, AK8963_CNTL, Mscale << 4 | Mmode); // Set magnetometer data resolution and sample ODR
    delay(10);
}

void SensorImpl::setupIMUDispositive()
{
    _gyroBias[0] = 0;
    _gyroBias[1] = 0;
    _gyroBias[2] = 0;
    _accelBias[0] = 0;
    _accelBias[1] = 0;
    _accelBias[2] = 0;
    _magCalibration[0] = 0;
    _magCalibration[1] = 0;
    _magCalibration[2] = 0;
    _magbias[0] = 0;
    _magbias[1] = 0;
    _magbias[2] = 0;

    calibrateIMU(_gyroBias, _accelBias);
    delay(1000);
    initIMU();
    delay(1000);
    initMagnetometer(_magCalibration);

    // IRF Observatory Data
    _magbias[0] = +520.;  // User environmental x-axis correction in milliGauss, should be automatically calculated
    _magbias[1] = +0.9;  // User environmental x-axis correction in milliGauss
    _magbias[2] = +105.5;  // User environmental x-axis correction in milliGauss

}

uint32_t SensorImpl::readPressureSensor(int address, byte code)
{
    uint32_t rawData = 0;

    Wire1.beginTransmission(address);
    Wire1.write(code);
    Wire1.endTransmission();
    delay(10);

    // Start read sequence
    Wire1.beginTransmission(address);
    Wire1.write(0x00);
    Wire1.endTransmission();
    Wire1.beginTransmission(address);
    Wire1.requestFrom(address, 3);
    if (Wire1.available() >= 3){
        uint16_t ret  = Wire1.read();
        rawData = 65536 * ret;
        ret = Wire1.read();
        rawData = rawData + 256 * ret;
        ret = Wire1.read();
        rawData = rawData + ret;
    }

    return rawData;
}

void SensorImpl::sendI2CCommand(int address, uint8_t cmd)
{
    Wire.beginTransmission(address);
    Wire.write(cmd);
    Wire.endTransmission();
}

void SensorImpl::sendI2CCommand16(int address, uint16_t cmd)
{
    Wire1.beginTransmission(address);
    Wire1.write(cmd >> 8);
    Wire1.write(cmd & 0xFF);
    Wire1.endTransmission();
}

void SensorImpl::updateEncoder1Callback()
{
    _encoderValue1++;
}

void SensorImpl::updateEncoder2Callback()
{
    _encoderValue2++;
}

void SensorImpl::resetI2Cbus()
{
    TWI1->TWI_CR = TWI_CR_SVDIS | TWI_CR_MSDIS; // Disable Master and Slave Mode;

    PMC->PMC_PCER0  = PMC_PCER0_PID12;    // PIOB power ON
    PIOB->PIO_PER  |= PIO_PER_P13;        // TWCK1 pin (SCL0) back to GPIO
    PIOB->PIO_OER  |= PIO_OER_P13;
    PIOB->PIO_OWER |= PIO_OWER_P13;

    PIOB->PIO_PER  |= PIO_PER_P12;        // TWCK1 pin (SDA0) back to GPIO
    PIOB->PIO_OER  |= PIO_OER_P12;
    PIOB->PIO_OWER |= PIO_OWER_P12;

    /*
     *  Generate 9 clock pulses to force slave release.
     */

    for (byte i=0; i<10; i++)
    {
        digitalWrite(SCL_PIN, HIGH);
        delayMicroseconds(50);
        digitalWrite(SDA_PIN, HIGH);
        delayMicroseconds(50);
    }

    /*
     *  Send the STOP over the line.
     */
    digitalWrite(SCL_PIN, LOW);
    digitalWrite(SDA_PIN, LOW);
    delay(20);
    digitalWrite(SCL_PIN, HIGH);
    digitalWrite(SDA_PIN, HIGH);

    /*
     *  Change the mode back to TWI from GPIO
     */
    // PMC->PMC_PCDR0 = PMC_PCDR0_PID12
    PIOB->PIO_PDR |= PIO_PDR_P12 | PIO_PDR_P13;            // Disable GPIO peripheral control

    PIOB->PIO_ABSR &= ~(PIO_PA17A_TWD0 | PIO_PA18A_TWCK0);      // TWD0 & TWCK0 Peripherals type A

    TWI0->TWI_CR = TWI_CR_MSEN;

    delay(30);

    Wire.begin();
    Wire.setClock(WIRE_CLOCK);
}


uint8_t crc8(const uint8_t *data, int len)
{
    const uint8_t POLYNOMIAL(0x31);
    uint8_t crc(0xFF);

    for (int j = len; j; --j){
        crc ^= *data++;

        for (int i = 8; i; --i){
            crc = (crc & 0x80) ? (crc << 1) ^POLYNOMIAL : (crc << 1);
        }
    }
    return crc;
}