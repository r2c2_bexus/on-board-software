/* 
  r2c2_sensor.h - Library in charge of communicate with all the sensors on board of R2C2 BEXUS experiment.
  Created by Guillermo Zaragoza Prous, May 22, 2019.
  Released into the public domain.
*/

#ifndef Sensor_lib_h
#define Sensor_lib_h

//#include <Arduino.h>
#include <Wire.h>
#include "r2c2_store_lib.h"
#include <r2c2_sensor_pins.h>
//#include "HardwareSerial.h"

#define NUM_MAX_PULSES_ENCODER_1         68000
#define NUM_MAX_PULSES_ENCODER_2          6500

class TempSensorAddr
{
public:
    enum
    {
        TEMP_1 = 0x48,
        TEMP_2 = 0x49,
        TEMP_3 = 0x4A,
        TEMP_4 = 0x4B,
        TEMP_5 = A10
    };
};

class PressSensorAddr
{
public:
    enum
    {
        PRESS_1 = 0x77
    };
};

class HumidSensorAddr
{
public:
    enum
    {
        HUMID_1 = 0x45
    };
};

class EncoderSensorAddr
{
public:
    enum
    {
        ENCODER_1 = 6,
        ENCODER_2 = 5
    };
};

class IMUSensorAddr
{
public:
    enum
    {
        IMU_1 = 0x68
    };
};

class MagnetometerSensorAddr
{
public:
    enum
    {
        MAG_1 = 0x0C
    };
};

class CameraSensorAddr
{
public:
    enum
    {
        CAMERA_1 = 28
    };
};

class Sensor
{
public:

    struct Position
    {
        float longitude;
        float latitude;
        unsigned int altitude;
        bool validity;
        Position(float newLongitude, float newLatitude, unsigned int newAltitude, bool isValid = false);
    };

    struct Inclination
    {
        float accX, accY, accZ;
        float magX, magY, magZ;
        float gyrX, gyrY, gyrZ;
        bool validity;
        Inclination(float newAccX, float newAccY, float newAccZ,
                    float newMagX, float newMagY, float newMagZ,
                    float newGyrX, float newGyrY, float newGyrZ,
                    bool isValid = false);
    };

    Sensor(Store * store);

    virtual float getTemperature(int address) = 0;
    virtual float getThermistorTemperature() = 0;
    virtual Sensor::Position getPosition() = 0;
    virtual float getHumidity() = 0;
    virtual unsigned int getPressure() = 0;
    virtual unsigned int getContainerState(const unsigned int numSensor) = 0;
    virtual uint32_t getEncoderState(const unsigned int sensorAddress) = 0;
    virtual bool isCameraRecording() = 0;

    virtual Sensor::Inclination getInclination() = 0;

    virtual void resetI2Cbus() = 0;

protected:

    Store *_store;
};

class SensorImpl : public Sensor
{
	public:

		SensorImpl(Store * store);

        float getTemperature(int address);
        float getThermistorTemperature();
		Sensor::Position getPosition();
        float getHumidity();
        unsigned int getPressure();
        unsigned int getContainerState(const unsigned int numSensor);
        uint32_t getEncoderState(const unsigned int sensorAddress);
        bool isCameraRecording();

        Sensor::Inclination getInclination();

        void resetI2Cbus();

	private:

		HardwareSerial* _gpsSerial;

        uint16_t _pressureCoeficients[6];

        static volatile unsigned long _encoderValue1;

        static volatile unsigned long _encoderValue2;

        float getThermistor2Temperature(); // Former temperature sensor 5, for the servo's box

        uint32_t readPressureSensor(int address, byte code);

        void setupSerialGPSDispositive();
		void setupI2CTempDispositive(int address);
        void setupI2CPressDispositive(int address);
        void setupAnalogThermistorDispositive();

        void setupDetectorSwitchDispositive(int address);

        void setupInterruptionEncoderDispositive();

        void setupCameraSensorDispositive();

        float _gyroBias[3];
        float _accelBias[3];
        float _magCalibration[3];
        float _magbias[3];

        void IMUwriteByte(uint8_t address, uint8_t subAddress, uint8_t data);
        uint8_t IMUreadByte(uint8_t address, uint8_t subAddress);
        void IMUreadBytes(uint8_t address, uint8_t subAddress, uint8_t count, uint8_t *dest);

        void calibrateIMU(float *dest1, float *dest2);
        void initIMU();
        void initMagnetometer(float *destination);
        void setupIMUDispositive();

        void readAccelData(int16_t *destination);
        void readGyroData(int16_t *destination);
        void readMagData(int16_t *destination);

        void sendI2CCommand(int address, uint8_t cmd);
        void sendI2CCommand16(int address, uint16_t cmd);

        static void updateEncoder1Callback();
        static void updateEncoder2Callback();
};

#endif // Sensor_lib_h