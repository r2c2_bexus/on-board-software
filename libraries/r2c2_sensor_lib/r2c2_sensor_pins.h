//
// Created by guillermo on 25/07/19.
//

#ifndef ON_BOARD_SOFTWARE_R2C2_SENSOR_PINS_H
#define ON_BOARD_SOFTWARE_R2C2_SENSOR_PINS_H

#define DETECTOR_SWITCH_1_PIN  A3
#define DETECTOR_SWITCH_2_PIN  A4
#define DETECTOR_SWITCH_3_PIN  A5

#define THERMISTOR_PIN         A8

#define RESET_I2C_PIN          42
#define SCL_PIN                21
#define SDA_PIN                20

#endif //ON_BOARD_SOFTWARE_R2C2_SENSOR_PINS_H
