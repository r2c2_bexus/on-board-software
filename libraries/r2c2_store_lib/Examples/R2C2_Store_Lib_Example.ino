#include <r2c2_store_impl_lib.h>

class Example
{
  public:

    // Constructor method
    Example(Store *store)
    {
      _store = store; 
    }
  
    void logSomething(){
      _store->log(INFO_LEVEL, "Logging INFO from Example!!");
    }

  private:    
    // Level of logging
    Store *_store;

};


String fileName = "STORE";
const int fileDuration = 25;
int id;

// Pointer to the Store instance to be used during the whole programme.
Store* store_lib = NULL; 

// Example of how to use the Store instance.
Example* example = NULL;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  id = 0;
  Serial.println("Starting...");
   
  delay(2000);
  Serial.print("Time duration for a file: ");
  Serial.println(fileDuration);
  store_lib = new StoreImpl(fileName, fileDuration, TRACE_LEVEL);
  example = new Example(store_lib);
}

void loop() {
  // put your main code here, to run repeatedly:
  store_lib->storeData("This is a message!!", id); 
  delay(100); 
  store_lib->log(TRACE_LEVEL, "This is a logged trace!!");
  delay(100);
  store_lib->log(INFO_LEVEL, "This is logged info!!");
  delay(100);
  store_lib->log(ERROR_LEVEL, "This is a logged error!!");
  delay(100);
  store_lib->log(DEBUG_LEVEL, "This is a logged debug message!!");
  delay(100);

  example->logSomething();
  delay(500);
  
  id = id + 1;
}
