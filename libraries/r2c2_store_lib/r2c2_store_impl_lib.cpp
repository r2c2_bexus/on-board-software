/*
  r2c2_store_lib.cpp - Library for storing information on board of R2C2 BEXUS experiment.
  Created by Guillermo Zaragoza Prous, February 23, 2019.
  Released into the public domain.
*/

#include "Arduino.h"
#include "r2c2_store_impl_lib.h"
#include <TimeLib.h>

int numberOfFile = 0; 

// Constructor method
StoreImpl::StoreImpl(String nameBase, int timeLimit, int logLevel) : Store(nameBase, timeLimit, logLevel)
{
	_SDCardDetected = false;
	
	Serial.println("Initializating SD card...");
	initializeSDCard();
    openFile();
}

// Function to store data from sensors
void StoreImpl::storeData(String value, int sensorNumber)
{
	// Concatenate timestamp, value and sensorNumber to the message
	String separator = " || ";
	String message = dateTimeNow() + separator + sensorNumber + separator + value;

	writeFile(message);

	return;
}

// Function to log information depending on the level of severity
void StoreImpl::log(int level, String message)
{
	if(_logLevel < level) return;

	// Concatenate timestamp to the message	
	String separator = " || ";
	writeFile(dateTimeNow() + separator + message);
	
	return;
}

void StoreImpl::initializeSDCard()
{
	if(!_SDCardDetected)
	{
		_SDCardDetected = SD.begin(_pinSDCardSlot);
		Serial.println(String("SD Card ") + ( _SDCardDetected ? "" : "not " ) + "detected!"); 
	}
}
	
// Opens a new file
int StoreImpl::openFile()
{
	initializeSDCard();
	
	if(!_SDCardDetected) return 1;
    
	String fileName = _nameBase + numberOfFile + _fileExtension;
	_dataFile = SD.open(fileName, FILE_WRITE);
    _dataFile.println("+++++++++++++++++++++++++++++++++++++++++");
	
	_timeLastFileWasCreated = getTimeInSeconds();
	Serial.println("New file opened.");
	return 0;
}

// Closes the file if opened
int StoreImpl::closeFile()
{
	initializeSDCard();
	
	if(!_SDCardDetected) return 1;

	if(_dataFile)
	{
        _dataFile.println("-----------------------------------------");
		_dataFile.close();
		Serial.println("File closed.");
		return 0;
	}
	return 1;
}

// Write in file
int StoreImpl::writeFile(String message)
{
	initializeSDCard();
	
	if(!_SDCardDetected) return 1;

	checkAndRotateFile();

	//Serial.println("ST: " + message);
	_dataFile.println(message);

	return 0;
}
    
int StoreImpl::checkAndRotateFile()
{
	if(!_SDCardDetected) return 1;

	if (getTimeInSeconds() - _timeLastFileWasCreated >= _timeLimit)
	{
		closeFile();
		numberOfFile++;
		openFile();
	}

	return 0;
}

String StoreImpl::dateTimeNow()
{
	time_t t = now();
	String dateTime(year(t));
	dateTime += String("/");
	dateTime += String(month(t));
	dateTime += String("/");
	dateTime += String(day(t));
	dateTime += String("-");
	dateTime += String(hour(t));
	dateTime += String(":");
	dateTime += String(minute(t));
	dateTime += String(":");
	dateTime += String(second(t));
	return dateTime;

}

int StoreImpl::getTimeInSeconds()
{
	time_t t = now();
	return hour(t) * 3600 + minute(t) * 60 + second(t);
}
