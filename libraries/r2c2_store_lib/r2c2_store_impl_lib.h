/*
  r2c2_store_lib.h - Library for storing information on board of R2C2 BEXUS experiment.
  Created by Guillermo Zaragoza Prous, February 23, 2019.
  Released into the public domain.
*/
#ifndef Store_Impl_lib_h
#define Store_Impl_lib_h

#include "r2c2_store_lib.h"
#include <SPI.h>
#include <SD.h>


class StoreImpl : public Store
{
  public:

    // Constructor method
    StoreImpl(String nameBase, int timeLimit, int logLevel);
  
    // Function to store data from sensors
    void storeData(String value, int sensorNumber);

    // Functions to log information depending on the level of severity
    void log(int level, String message);

  private:

    // Functions to control the access to the SD Card and the current file
    void initializeSDCard();
    int openFile();
    int closeFile();
    int writeFile(String message);
    int checkAndRotateFile();

    // Returns the number of second that have passed since 00:00
    int getTimeInSeconds();

    // Returns the current date and time in String
    String dateTimeNow(); 

    // SD Card pin
    const int _pinSDCardSlot = 4;

    // SD Card current file
    File _dataFile;

    // SD Card detected
    bool _SDCardDetected;

};

#endif // Store_Impl_lib_h
