/*
  r2c2_store_lib.cpp - Library for storing information on board of R2C2 BEXUS experiment.
  Created by Guillermo Zaragoza Prous, February 23, 2019.
  Released into the public domain.
*/

#include "r2c2_store_lib.h"


// Constructor method
Store::Store(String nameBase, int timeLimit, int logLevel)
{
	_timeLimit = timeLimit;
	_timeLastFileWasCreated = 0;

	_nameBase = nameBase;
	_fileExtension = ".log";
	_logLevel = logLevel;
	
}

