/*
  r2c2_store_lib.h - Library for storing information on board of R2C2 BEXUS experiment.
  Created by Guillermo Zaragoza Prous, February 23, 2019.
  Released into the public domain.
*/
#ifndef Store_lib_h
#define Store_lib_h

#include "Arduino.h"

#define ERROR_LEVEL  0
#define INFO_LEVEL   1
#define DEBUG_LEVEL  2
#define TRACE_LEVEL  3

class Store
{
  public:

    // Constructor method
    Store(String nameBase, int timeLimit, int logLevel);
  
    // Function to store data from sensors
    virtual void storeData(String value, int sensorNumber) = 0;

    // Functions to log information depending on the level of severity
    virtual void log(int level, String message) = 0;

  protected: 
  
    // Time limit for the rotation of files
    int _timeLimit;

    // Time of the creation of the last storage file
    int _timeLastFileWasCreated;
    
    // Base for the name of the stored files
    String _nameBase;
    String _fileExtension;

    // Level of logging
    int _logLevel;

};

#endif // Store_lib_h
