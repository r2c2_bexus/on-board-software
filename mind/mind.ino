/**
  mind.ino - Main programme running on board the R2C2 BEXUS experiment.
  @author Guillermo Zaragoza Prous
  @date July 23, 2019

  Released into the public domain.
*/

#include <r2c2_communication_lib.h>
#include <r2c2_communication_protocol.h>
#include <r2c2_store_impl_lib.h>
#include <r2c2_data.h>
#include <r2c2_sensor_lib.h>
#include <r2c2_actuator_lib.h>
#include <r2c2_safety_lib.h>
 
// Instances of the R2C2 libraries' classes
Store* store = NULL;
Communicate* comm = NULL;

Sensor* sensor = NULL;
Actuator* actuator = NULL;

SensorData* sensorData = new SensorData();
StatusData* statusData = new StatusData();

Safety* safety = NULL;
Autonomous* autonomous = NULL;

// Definition of the variables needed for the storage of data on the SD card.
String fileName = "FLY";
const int FILE_DURATION = 180; // seconds
unsigned int numIMUStoresAfterBooting = 15000;

// Order of the containers to be released.
const int CONTAINER_ORDER[3] = {1, 3, 2};

/**
 * Setups all the environment for the correct execution of the program along with the other systems in the experiment.
 */
void setup() {
  Serial.begin(9600);
  
  store = new StoreImpl(fileName, FILE_DURATION, INFO_LEVEL);
  comm  = new CommunicateImpl(store);

  sensor = new SensorImpl(store);
  actuator = new ActuatorImpl(store);

  safety = new SafetyImpl(store);
  autonomous = new AutonomousImpl(store, actuator, sensor);

  // Common Ethernet configuration
  byte         mac[6]      = {0xde, 0xaa, 0x3f, 0x8d, 0x22, 0xa3};
  unsigned int ip[4]       = {172, 16, 18, 131};

  // UDP configuration
  unsigned int localUDPPort   = 6666;
  unsigned int remoteIp[4] = {172, 16, 18, 130};
  unsigned int remoteUDPPort  = 7777;

  // TCP configuration
  byte dns[] = { 172, 16, 18, 130 };
  byte gateway[] = { 172, 16, 18, 130 };
  byte subnet[] = { 255, 255, 255, 0 };
  unsigned int localTCPport = 8000;
  
  comm->prepareConnection(mac, ip, dns, gateway, subnet, localTCPport, localUDPPort, remoteIp, remoteUDPPort);
  
  checkConnection(statusData);

  statusData->setStateMachineStatus(MANUAL);
  
}

/**
 * Main loop of the execution. All the business logic is inside this function. For more information about the flow, check
 * the SED document of the team.
 */
void loop() 
{
  safety->getValueAndCheckSensorStatuses(sensor, sensorData, statusData);
  safety->checkActuatorStatuses(actuator, sensor, statusData);
  safety->checkExperimentStatus(statusData);
  
  safeSensorData(sensorData, statusData);

  State_Machine_Status state = statusData->getStateMachineStatus();
  Communication_Status communicationStatus = checkConnection(statusData);

  if (communicationStatus == COM_DISCONNECTED && state == MANUAL)
  {
    statusData->setStateMachineStatus(AUTONOMOUS);
    state = statusData->getStateMachineStatus();
    store->log(INFO_LEVEL, "State machine changed to AUTONOMOUS from MANUAL as the TCP communication has been disconected.");
  }

  comm->sendSensorData(sensorData);
  checkCommand(state);

  switch(state)
  {
    case PASSIVE:
        turnOffAllActuatorsButHeaters();
        autonomous->checkTemperatureSensorStatusAndActuate(statusData);
        autonomous->checkAllTemperatures(sensorData, statusData);
      break;
      
    case MANUAL:
      if (statusData->getMotorStatus(1) == ACT_ON)
      {
        autonomous->checkAndStopReelingUpMotor(EncoderSensorAddr::ENCODER_1);
      }
      else if (statusData->getMotorStatus(2) == ACT_ON)
      {
        autonomous->checkAndStopReelingUpMotor(EncoderSensorAddr::ENCODER_2);
      }
      break;
      
    case AUTONOMOUS:
      
      bool safeToRelease = safety->checkPreReleaseSafetyRules(sensorData, statusData);
      static bool onGoingRelease = false;
      
      statusData->setReleasingStatus(onGoingRelease);

      if (!onGoingRelease)
      {
        autonomous->checkTemperatureSensorStatusAndActuate(statusData);
        autonomous->checkAllTemperatures(sensorData, statusData);
      }

      if (safeToRelease || onGoingRelease)
      {
        static int container = -1;
        
        if(!onGoingRelease)
        {
          container = nextContainer(statusData);
        }
        
        bool reelUpImmediately = !safety->checkCriticalSafetyRules(sensorData, container);
        if(container == 1 && safety->checkIfAscendingPhase(sensorData, statusData))
        {
          onGoingRelease = autonomous->releaseAContainer(container, sensorData, statusData, reelUpImmediately);
        }
        else if (container == 2 || container == 3)
        {
          onGoingRelease = autonomous->releaseAContainer(container, sensorData, statusData, reelUpImmediately);
        }
      }      

      if (statusData->getContainerStatus(1) == CONT_RELEASED &&
          statusData->getContainerStatus(2) == CONT_RELEASED &&
          statusData->getContainerStatus(3) == CONT_RELEASED &&
          !onGoingRelease)
      {
          statusData->setStateMachineStatus(PASSIVE);
      }
      break;
      
  }

  comm->sendStatusData(statusData);
  safeStatusData(statusData);

  sensor->getEncoderState(EncoderSensorAddr::ENCODER_1);
  sensor->getEncoderState(EncoderSensorAddr::ENCODER_2);
}


/**
    Checks which is the following container to be released

    @param statusData status of the experiment
    @return the index of the following container to be released. -1 if no remaining.
*/
int nextContainer(const StatusData* statusData)
{
  static int newContainer = -1;

  bool found = false;
   
  for (int i = 0; i < 3 && !found; i++)
  {
    if (statusData->getContainerStatus(CONTAINER_ORDER[i]) != CONT_RELEASED)
    {
      if (CONTAINER_ORDER[i] != 1 || safety->checkIfAscendingPhase(sensorData, statusData))
      {
        newContainer = CONTAINER_ORDER[i];
        found = true;
      }
    }
  }

  if(!found)
  {
    newContainer = -1;
  }
  
  return newContainer;
}

/**
    Stores the given values from the sensors into the SD card. The status data is needed as
    the IMU data is only stored when the camera is recording.

    @param sensorData current data from the sensors
    @param statusData status of the experiment
*/
void safeSensorData(const SensorData* sensorData, const StatusData* statusData)
{
  store->log(INFO_LEVEL, "Storing current sensor values...");
  store->storeData(String(sensorData->getTemperature(1)), TEMPERATURE_SENS_1_ID);
  store->storeData(String(sensorData->getTemperature(2)), TEMPERATURE_SENS_2_ID);
  store->storeData(String(sensorData->getTemperature(3)), TEMPERATURE_SENS_3_ID);
  store->storeData(String(sensorData->getTemperature(4)), TEMPERATURE_SENS_4_ID);
  store->storeData(String(sensorData->getTemperature(5)), TEMPERATURE_SENS_5_ID);
  store->storeData(String(sensorData->getTemperature(6)), TEMPERATURE_SENS_6_ID);
  store->storeData(String(sensorData->getHumidity()), HUMIDITY_SENS_ID);
  store->storeData(String(sensorData->getLongitude()), GPS_ID);
  store->storeData(String(sensorData->getLatitude()), GPS_ID);
  store->storeData(String(sensorData->getAltitude()), GPS_ID);
  store->storeData(String(sensorData->getPressure()), PRESSURE_SENS_ID);

  if(statusData->getCameraStatus() == CAM_RECORDING || (numIMUStoresAfterBooting > 0))
  {
    store->storeData(String(sensorData->getAccX()), IMU_SENS_ID);
    store->storeData(String(sensorData->getAccY()), IMU_SENS_ID);
    store->storeData(String(sensorData->getAccZ()), IMU_SENS_ID);
    store->storeData(String(sensorData->getMagX()), IMU_SENS_ID);
    store->storeData(String(sensorData->getMagY()), IMU_SENS_ID);
    store->storeData(String(sensorData->getMagZ()), IMU_SENS_ID);
    store->storeData(String(sensorData->getGyrX()), IMU_SENS_ID);
    store->storeData(String(sensorData->getGyrY()), IMU_SENS_ID);
    store->storeData(String(sensorData->getGyrZ()), IMU_SENS_ID);
    numIMUStoresAfterBooting = (numIMUStoresAfterBooting > 0) ? numIMUStoresAfterBooting-1 : 0;
  }
  store->log(INFO_LEVEL, "Current sensor values stored.");
  
}

/**
    Stores the given values of the status of the experiment into the SD card.

    @param statusData status of the experiment
*/
void safeStatusData(const StatusData* statusData)
{
  store->log(INFO_LEVEL, "Storing current status values...");
  
  store->log(INFO_LEVEL, "EXPERIMENT STATUS: " + String(statusData->getExperimentStatus()));
  store->log(INFO_LEVEL, "CONTAINER 1 STATUS: " + String(statusData->getContainerStatus(1)));
  store->log(INFO_LEVEL, "CONTAINER 2 STATUS: " + String(statusData->getContainerStatus(2)));
  store->log(INFO_LEVEL, "CONTAINER 3 STATUS: " + String(statusData->getContainerStatus(3)));
  
  store->log(INFO_LEVEL, "TEMP SENSOR 1 STATUS: " + String(statusData->getTemperatureSensorStatus(1)));
  store->log(INFO_LEVEL, "TEMP SENSOR 2 STATUS: " + String(statusData->getTemperatureSensorStatus(2)));
  store->log(INFO_LEVEL, "TEMP SENSOR 3 STATUS: " + String(statusData->getTemperatureSensorStatus(3)));
  store->log(INFO_LEVEL, "TEMP SENSOR 4 STATUS: " + String(statusData->getTemperatureSensorStatus(4)));
  store->log(INFO_LEVEL, "TEMP SENSOR 5 STATUS: " + String(statusData->getTemperatureSensorStatus(5)));
  store->log(INFO_LEVEL, "TEMP SENSOR 6 STATUS: " + String(statusData->getTemperatureSensorStatus(6)));  
  store->log(INFO_LEVEL, "HUMID SENSOR STATUS: " + String(statusData->getHumiditySensorStatus()));  
  store->log(INFO_LEVEL, "GPS SENSOR STATUS: " + String(statusData->getGPSStatus()));
  store->log(INFO_LEVEL, "PRESSURE SENSOR STATUS: " + String(statusData->getPressureStatus()));
  store->log(INFO_LEVEL, "IMU SENSOR STATUS: " + String(statusData->getIMUStatus()));

  store->log(INFO_LEVEL, "STATE MACHINE STATUS: " + String(stateMachineToString(statusData->getStateMachineStatus())));
  store->log(INFO_LEVEL, "COMMUNICATION STATUS: " + String(statusData->getCommunicationStatus()));
  store->log(INFO_LEVEL, "IS RELEASING: " + String(statusData->isReleasing()));
  
  store->log(INFO_LEVEL, "CAMERA STATUS: " + String(statusData->getCameraStatus()));
  store->log(INFO_LEVEL, "HEATER 1 STATUS: " + String(statusData->getHeaterStatus(1)));
  store->log(INFO_LEVEL, "HEATER 2 STATUS: " + String(statusData->getHeaterStatus(2)));
  store->log(INFO_LEVEL, "HEATER 3 STATUS: " + String(statusData->getHeaterStatus(3)));
  store->log(INFO_LEVEL, "HEATER 4 STATUS: " + String(statusData->getHeaterStatus(4)));
  store->log(INFO_LEVEL, "HEATER 5 STATUS: " + String(statusData->getHeaterStatus(5)));

  store->log(INFO_LEVEL, "MOTOR 1 STATUS: " + String(statusData->getMotorStatus(1)));
  store->log(INFO_LEVEL, "MOTOR 2 STATUS: " + String(statusData->getMotorStatus(2)));
  
  store->log(INFO_LEVEL, "SERVO 1 STATUS: " + String(statusData->getServoStatus(1)));
  store->log(INFO_LEVEL, "SERVO 2 STATUS: " + String(statusData->getServoStatus(2)));
  store->log(INFO_LEVEL, "SERVO 3 STATUS: " + String(statusData->getServoStatus(3)));

  store->log(INFO_LEVEL, "LAUNCH TIME: " + String(statusData->getLaunchTime()));
  store->log(INFO_LEVEL, "RELEASE GAP TIME: " + String(statusData->getReleaseGapTime()));
  store->log(INFO_LEVEL, "CONTAINER 1 RELEASE TIME: " + String(statusData->getContainerReleaseTime(1)));
  store->log(INFO_LEVEL, "CONTAINER 2 RELEASE TIME: " + String(statusData->getContainerReleaseTime(2)));
  store->log(INFO_LEVEL, "CONTAINER 3 RELEASE TIME: " + String(statusData->getContainerReleaseTime(3)));
  store->log(INFO_LEVEL, "LAST CONTAINER RELEASED: " + String(statusData->getLastContainerReleased()));
  store->log(INFO_LEVEL, "LAST CONTAINER RELEASED TIME: " + String(statusData->getLastContainerReleasedTime()));
    
  store->log(INFO_LEVEL, "Current status values stored.");
  
}


/**
 * Checks the status of the connection for the uplink and sets the communication status in the status data
 * @param statusData status of the experiment
 * @return the communication status
 */
Communication_Status checkConnection(StatusData* statusData)
{
  bool uplinkConnected = comm->isUplinkConnected();
  if (!uplinkConnected)
  {
    statusData->setCommunicationStatus(COM_DISCONNECTED);
  }
  else
  {
      statusData->setCommunicationStatus(COM_CONNECTED);
  }


  Communication_Status communicationStatus = statusData->getCommunicationStatus();

  if (communicationStatus == COM_DISCONNECTED)
  {
     int connected = comm->startConnection();
  }

  return communicationStatus;
}

/**
 * Check if there is a new received command. In that case, checks if it can be processed in the current mode and proceeds
 * to actuate accordingly.
 * @param state status of the state machine
 */
void checkCommand(State_Machine_Status state)
{
  if(comm->commandAvailable())
    {
      store->log(INFO_LEVEL, "Command available.");
      byte commandCode = 0x00;
      uint16_t releaseGapTime = 0;
      int receiveCommandResult = comm->processMessage(commandCode, releaseGapTime, statusData);
      if(receiveCommandResult == COMM_OK)
      {
        switch(state)
        {
          case PASSIVE:
          case AUTONOMOUS:
            processCommandInNotManualMode(commandCode);
            break;
          case MANUAL:
            processCommand(commandCode, releaseGapTime);
            break;
        }
      }
      else
      {
        store->log(INFO_LEVEL, "Received command could not be processed.");
      }
    }
}

/**
 * Processes the given command.
 * @param commandCode Code of the command to be processed.
 * @param releaseGap argument only valid for the "set release gap" command. It is expected to be in seconds.
 */
void processCommand(byte& commandCode, uint16_t& releaseGap)
{

  if (commandCode      == CMD_PASSIVE_MODE)
  {
    store->log(INFO_LEVEL, "State Machine set to PASSIVE mode.");
    statusData->setStateMachineStatus(PASSIVE);
  }
  else if(commandCode  == CMD_MANUAL_MODE)
  {
    store->log(INFO_LEVEL, "State Machine set to MANUAL mode.");
    statusData->setStateMachineStatus(MANUAL);
  }
  else if(commandCode  == CMD_AUTONOMOUS_MODE)
  {
    store->log(INFO_LEVEL, "State Machine set to AUTONOMOUS mode.");
    statusData->setStateMachineStatus(AUTONOMOUS);
  }
  else if(commandCode  == CMD_RELEASE_CONTAINER_1)
  {
    store->log(INFO_LEVEL, "Releasing container 1.");
    actuator->servo(ServoAddr::SERVO_1, true);
    statusData->setContainerReleaseTime(1, now());
    statusData->setContainerStatus(1, CONT_RELEASED);
  }
  else if(commandCode  == CMD_RELEASE_CONTAINER_2)
  {
    store->log(INFO_LEVEL, "Releasing container 2.");
    actuator->servo(ServoAddr::SERVO_2, true);
    statusData->setContainerReleaseTime(2, now());
    statusData->setContainerStatus(2, CONT_RELEASED);
  }
  else if(commandCode  == CMD_RELEASE_CONTAINER_3)
  {
    store->log(INFO_LEVEL, "Releasing container 3.");
    actuator->servo(ServoAddr::SERVO_3, true);
    statusData->setContainerReleaseTime(3, now());
    statusData->setContainerStatus(3, CONT_RELEASED);
  }
  else if(commandCode  == CMD_RETRACT_SERVO_1)
  {
    store->log(INFO_LEVEL, "Retracting servo 1.");
    actuator->servo(ServoAddr::SERVO_1, false);
  }
  else if(commandCode  == CMD_RETRACT_SERVO_2)
  {
    store->log(INFO_LEVEL, "Retracting servo 2.");
    actuator->servo(ServoAddr::SERVO_2, false);
  }
  else if(commandCode  == CMD_RETRACT_SERVO_3)
  {
    store->log(INFO_LEVEL, "Retracting servo 3.");
    actuator->servo(ServoAddr::SERVO_3, false);
  }
  else if(commandCode  == CMD_ACTIVATE_MOTOR_1)
  {
    store->log(INFO_LEVEL, "Activating Motor 1.");
    actuator->motor(MotorAddr::MOTOR_1, true);
  }
  else if(commandCode  == CMD_DEACTIVATE_MOTOR_1)
  {
    store->log(INFO_LEVEL, "Deactivating Motor 1.");
    actuator->motor(MotorAddr::MOTOR_1, false);
  }
  else if(commandCode  == CMD_ACTIVATE_MOTOR_2)
  {
    store->log(INFO_LEVEL, "Activating Motor 2.");
    actuator->motor(MotorAddr::MOTOR_2, true);
  }
  else if(commandCode  == CMD_DEACTIVATE_MOTOR_2)
  {
    store->log(INFO_LEVEL, "Deactivating Motor 2.");
    actuator->motor(MotorAddr::MOTOR_2, false);
  }
  else if(commandCode  == CMD_ACTIVATE_HEATER_1)
  {
    store->log(INFO_LEVEL, "Activating Heater 1.");
    actuator->heater(HeaterAddr::HEATER_1, true);
  }
  else if(commandCode  == CMD_DEACTIVATE_HEATER_1)
  {
    store->log(INFO_LEVEL, "Deactivating Heater 1.");
    actuator->heater(HeaterAddr::HEATER_1, false);
  }
  else if(commandCode  == CMD_ACTIVATE_HEATER_2)
  {
    store->log(INFO_LEVEL, "Activating Heater 2.");
    actuator->heater(HeaterAddr::HEATER_2, true);
  }
  else if(commandCode  == CMD_DEACTIVATE_HEATER_2)
  {
    store->log(INFO_LEVEL, "Deactivating Heater 2.");
    actuator->heater(HeaterAddr::HEATER_2, false);
  }
  else if(commandCode  == CMD_ACTIVATE_HEATER_3)
  {
    store->log(INFO_LEVEL, "Activating Heater 3.");
    actuator->heater(HeaterAddr::HEATER_3, true);
  }
  else if(commandCode  == CMD_DEACTIVATE_HEATER_3)
  {
    store->log(INFO_LEVEL, "Deactivating Heater 3.");
    actuator->heater(HeaterAddr::HEATER_3, false);
  }
  else if(commandCode  == CMD_ACTIVATE_HEATER_4)
  {
    store->log(INFO_LEVEL, "Activating Heater 4.");
    actuator->heater(HeaterAddr::HEATER_4, true);
  }
  else if(commandCode  == CMD_DEACTIVATE_HEATER_4)
  {
    store->log(INFO_LEVEL, "Deactivating Heater 4.");
    actuator->heater(HeaterAddr::HEATER_4, false);
  }
  else if(commandCode  == CMD_ACTIVATE_HEATER_5)
  {
    store->log(INFO_LEVEL, "Activating Heater 5.");
    actuator->heater(HeaterAddr::HEATER_5, true);
  }
  else if(commandCode  == CMD_DEACTIVATE_HEATER_5)
  {
    store->log(INFO_LEVEL, "Deactivating Heater 5.");
    actuator->heater(HeaterAddr::HEATER_5, false);
  }
  else if(commandCode  == CMD_CHANGE_RELEASE_GAP_TIME)
  {
    store->log(INFO_LEVEL, "Changing Release Gap Time to " + String(releaseGap) + ".");
    statusData->setReleaseGapTime(releaseGap);
  }
  else if(commandCode  == CMD_CAMERA_MANAGER_ON)
  {
    store->log(INFO_LEVEL, "Activating Camera Manager.");
    actuator->cameraManagerPower(true);
  }
  else if(commandCode  == CMD_CAMERA_MANAGER_OFF)
  {
    store->log(INFO_LEVEL, "Deactivating Camera Manager.");
    actuator->cameraManagerPower(false);
  }
  else if(commandCode  == CMD_CAMERA_MANAGER_REC)
  {
    store->log(INFO_LEVEL, "Recording from Camera Manager.");
    actuator->cameraManagerRecord(true);
  }
  else if(commandCode  == CMD_CAMERA_MANAGER_STOP)
  {
    store->log(INFO_LEVEL, "Stop Camera Manager.");
    actuator->cameraManagerRecord(false);
  }
  else if(commandCode  == CMD_RESET_I2C_BUS)
  {
    store->log(INFO_LEVEL, "Reset I2C Bus.");
    sensor->resetI2Cbus();
  }

}

/**
 * Processes the given command.
 * @param commandCode
 */
void processCommandInNotManualMode(byte& commandCode)
{
  if (commandCode      == CMD_PASSIVE_MODE)
  {
    store->log(INFO_LEVEL, "State Machine set to PASSIVE mode.");
    statusData->setStateMachineStatus(PASSIVE);
  }
  else if(commandCode  == CMD_MANUAL_MODE)
  {
    store->log(INFO_LEVEL, "State Machine set to MANUAL mode.");
    statusData->setStateMachineStatus(MANUAL);
  }
  else if(commandCode  == CMD_AUTONOMOUS_MODE)
  {
    store->log(INFO_LEVEL, "State Machine set to AUTONOMOUS mode.");
    statusData->setStateMachineStatus(AUTONOMOUS);
  }
}

/**
 * Sets all the onboard actuators to off, but heaters.
 */
void turnOffAllActuatorsButHeaters()
{
    actuator->servo(ServoAddr::SERVO_1, false);
    actuator->servo(ServoAddr::SERVO_2, false);
    actuator->servo(ServoAddr::SERVO_3, false);

    actuator->motor(MotorAddr::MOTOR_1, false);
    actuator->motor(MotorAddr::MOTOR_2, false);
  
    actuator->cameraManagerPower(false);
}
